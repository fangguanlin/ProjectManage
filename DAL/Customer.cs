﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DBUtility;
using IDAL;
using Model;

namespace DAL
{
    public class Customer : BaseRepository<CustomerInfo>, ICustomer
    {
        private const string PARM_CUSTOMER_ID = "@CustomerID";
        private const string PARM_CUSTOMER_NAME = "@CustomerName";

        public IList<CustomerInfo> LoadEntities()
        {
            return ExcuteSqlToGetEntities("Customer_SelectAll");
        }

        public CustomerInfo LoadEntity<T1>(T1 customerID)
        {
            SqlParameter parm = GetParameter();
            parm.Value = customerID;
            return ExcuteSqlToGetEntity("Customer_Select", parm);
        }

        public CustomerInfo Customer_GetbyName(string customerName)
        {
            SqlParameter parm = new SqlParameter(PARM_CUSTOMER_NAME, SqlDbType.NVarChar, 50);
            parm.Value = customerName;
            return ExcuteSqlToGetEntity("Customer_SelectbyName", parm);
        }

        //-------------------------------------重写超类基础操作---------------------------------------------------------

        public override CustomerInfo GetEntityRecord(SqlDataReader rdr)
        {
            CustomerInfo pro = new CustomerInfo(rdr.GetInt32(0), rdr.GetValue(1).ToString());
            return pro;
        }

        public override CustomerInfo GetEntityRecord()
        {
            CustomerInfo pro = new CustomerInfo();
            return pro;
        }

        public override string GetInsertsp()
        {
            return "Customer_Insert";
        }

        public override string GetUpdatesp()
        {
            return null;
        }

        public override string GetDeletesp()
        {
            return "Customer_Delete";
        }

        public override SqlParameter GetParameter()
        {
            SqlParameter Parm = new SqlParameter(PARM_CUSTOMER_ID, SqlDbType.Int, 4);
            return Parm;
        }

        public override SqlParameter[] GetParameters()
        {
            SqlParameter[] parms = null;
            parms = new SqlParameter[]
                {
					new SqlParameter(PARM_CUSTOMER_ID, SqlDbType.Int, 4),
                    new SqlParameter(PARM_CUSTOMER_NAME, SqlDbType.NVarChar, 50)
                };
            return parms;
        }

        public override void SetParameters(CustomerInfo pro, params SqlParameter[] Parms)
        {
            Parms[0].Value = pro.CustomerID;
            Parms[1].Value = pro.CustomerName;
        }
    }
}
