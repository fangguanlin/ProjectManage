﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DBUtility;
using IDAL;
using Model;

namespace DAL
{
    public class Project : BaseRepository<ProjectInfo>, IProject
    {
        private const string PARM_PROJECT_PROJECTID = "@ProjectID";
        private const string PARM_PROJECT_PROJECTNAME = "@ProjectName";
        private const string PARM_PROJECT_USERID = "@UserID";
        private const string PARM_PROJECT_TEAM = "@ProjectTeam";
        private const string PARM_PROJECT_STEP = "@ProjectStep";
        private const string PARM_PROJECT_LEVEL = "@ProjectLevel";
        private const string PARM_PROJECT_ISIMPORTANT = "@IsImportant";

        private const string PARM_PROJECTITEM_DT = "@ProductTypeItemdt";

        private const string PARM_PROJECT_SEARCHFACTOR = "@SearchFactor";

        public IList<ProjectInfo> LoadEntities()
        {
            return ExcuteSqlToGetEntities("Project_SelectAll");
        }

        /// <summary>
        /// Get an individual category based on a provided id
        /// </summary>
        /// <param name="categoryId">Category id</param>
        /// <returns>Details about the Category</returns>
        public ProjectInfo LoadEntity<T1>(T1 projectID)
        {
            SqlParameter parm = GetParameter();
            parm.Value = projectID;
            return ExcuteSqlToGetEntity("Project_Select", parm);
        }

        public IList<ProjectInfo> Project_GetbyClose()
        {
            return ExcuteSqlToGetEntities("Project_SelectClose");
        }
        
        public bool ProjectItems_Insert(ProjectInfo proi, DataTable itemdt)
        {
            List<SqlParameter> ItemList = new List<SqlParameter>() {
            sqlHelper.CreateSqlParemeterStructured(PARM_PROJECTITEM_DT,itemdt)};
            SqlParameter[] Parms = GetParameters();
            SetParameters(proi, Parms);

            SqlCommand cmd = new SqlCommand();
            foreach (SqlParameter parm in Parms)
                cmd.Parameters.Add(parm);
            foreach (SqlParameter parm in ItemList)
                cmd.Parameters.Add(parm);

            using (SqlConnection conn = new SqlConnection(sqlHelper.ConnectionStringLocalTransaction))
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                try
                {
                    cmd.CommandText = "Project_Insert";
                    using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection)) { }
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public void Project_SetClose(string projectID)
        {
            SqlParameter parm = GetParameter();
            parm.Value = projectID;

            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.Add(parm);
            ExecuteCommand(cmd, "Project_SetClose");
        }

        public IList<ProjectInfo> Project_GetMyProject(string userID)
        {
            SqlParameter parm = new SqlParameter(PARM_PROJECT_USERID, SqlDbType.NVarChar, 20);
            parm.Value = userID;
            return ExcuteSqlToGetEntities("Project_SelectMyProject", parm);
        }

        public IList<ProjectInfo> Project_GetbySearch(string searchFactor)
        {
            SqlParameter parm = new SqlParameter(PARM_PROJECT_SEARCHFACTOR, SqlDbType.NVarChar, 50);
            parm.Value = string.IsNullOrWhiteSpace(searchFactor) ? null : searchFactor; 
            return ExcuteSqlToGetEntities("Project_SelectbySearch",parm);
        }


        //-------------------------------------重写超类基础操作---------------------------------------------------------

        public override ProjectInfo GetEntityRecord(SqlDataReader rdr)
        {
            ProjectInfo pro = new ProjectInfo(rdr.GetValue(0).ToString(), rdr.GetValue(1).ToString(), rdr.GetValue(2).ToString()
                , rdr.GetDateTime(3), rdr.GetValue(4).ToString(),rdr.GetValue(5).ToString(),rdr.GetValue(6).ToString()
                ,rdr.GetValue(7).ToString(),rdr.GetBoolean(8));
            return pro;
        }

        public override ProjectInfo GetEntityRecord()
        {
            ProjectInfo pro = new ProjectInfo();
            return pro;
        }

        public override string GetInsertsp()
        {
            return null;
        }

        public override string GetUpdatesp()
        {
            return "Project_Update";
        }

        public override string GetDeletesp()
        {
            return "Project_Delete";
        }

        public override SqlParameter GetParameter()
        {
            SqlParameter Parm = new SqlParameter(PARM_PROJECT_PROJECTID, SqlDbType.NVarChar, 50);
            return Parm;
        }

        public override SqlParameter[] GetParameters()
        {
            SqlParameter[] parms = null;
            parms = new SqlParameter[]
                {
					new SqlParameter(PARM_PROJECT_PROJECTID, SqlDbType.NVarChar, 50),
                    new SqlParameter(PARM_PROJECT_PROJECTNAME, SqlDbType.NVarChar, 50),
					new SqlParameter(PARM_PROJECT_USERID, SqlDbType.NVarChar, 10),
                    new SqlParameter(PARM_PROJECT_TEAM,SqlDbType.NVarChar,100),
                    new SqlParameter(PARM_PROJECT_STEP,SqlDbType.NVarChar,50),
                    new SqlParameter(PARM_PROJECT_LEVEL,SqlDbType.NVarChar,50),
                    new SqlParameter(PARM_PROJECT_ISIMPORTANT,SqlDbType.Bit,1)
                };
            return parms;
        }

        public override void SetParameters(ProjectInfo pro, params SqlParameter[] Parms)
        {
            Parms[0].Value = pro.ProjectID;
            Parms[1].Value = pro.ProjectName;
            Parms[2].Value = pro.UserID;
            Parms[3].Value = pro.ProjectTeam;
            Parms[4].Value = pro.ProjectStep;
            Parms[5].Value = pro.ProjectLevel;
            Parms[6].Value = pro.IsImportant;
        }
    }
}
