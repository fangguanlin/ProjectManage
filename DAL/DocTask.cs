﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DBUtility;
using IDAL;
using Model;

namespace DAL
{
    public class DocTask : BaseRepository<DocTaskInfo>, IDocTask
    {
        private const string PARM_DOCTASK_DOCTASKID = "@TaskID";
        private const string PARM_DOCTASK_PROJECTID = "@ProjectID";
        private const string PARM_DOCTASK_TASKNAME = "@TaskName";
        private const string PARM_DOCTASK_PRODUCTTYPESTEP = "@ProductTypeStep";
        private const string PARM_DOCTASK_PUBLISHER = "@Publisher";
        private const string PARM_DOCTASK_BEGINTIME = "@BeginTime";
        private const string PARM_DOCTASK_POSTTIME = "@PostTime";
        private const string PARM_DOCTASK_EXECUTOR = "@Executor";
        private const string PARM_DOCTASK_DEPARTMENT = "@Department";
        private const string PARM_DOCTASK_TASKDETAIL = "@TaskDetail";
        private const string PARM_DOCTASK_TASKSTATUS = "@TaskStatus";

        private const string PARM_DOCTASK_PROJECTNAME = "@ProjectName";
        private const string PARM_DOCTASK_CUSTOMERID = "@CustomerID";
        private const string PARM_DOCTASK_PRODUCTTYPEID = "@ProductTypeID";

        private const string PARM_DOCTASK_STARTTIME = "@startTime";
        private const string PARM_DOCTASK_STOPTIME = "@stopTime";

        private const string PARM_DOCTASK_YEARTIME = "@YearTime";
        private const string PARM_DOCTASK_QUARTERTIME = "@QuarterTime";

        public IList<DocTaskInfo> LoadEntities()
        {
            return null;
        }

        /// <summary>
        /// Get an individual category based on a provided id
        /// </summary>
        /// <param name="categoryId">Category id</param>
        /// <returns>Details about the Category</returns>
        public DocTaskInfo LoadEntity<T1>(T1 taskID)
        {
            SqlParameter parm = GetParameter();
            //Bind the parameter
            parm.Value = taskID;
            return ExcuteSqlToGetEntity("DocTask_SelectbyID", parm);
        }

        public DocTaskInfo GetDocTaskbyPIDName(string projectID,string taskName)
        {
            SqlParameter[] parms = GetPIDNameParameter();
            parms[0].Value = projectID;
            parms[1].Value = taskName;
            return ExcuteSqlToGetEntity("DocTask_SelectbyPIDName", parms);
        }  
                
        public IList<DocTaskInfo> GetDocTasksbyPID(string projectID)
        {
            SqlParameter parm = GetPIDParameter();
            parm.Value = projectID;
            return ExcuteSqlToGetEntities("DocTask_SelectbyPID", parm);
        }

        public IList<DocTaskInfo> GetDocTasksbyPIDClose(string projectID)
        {
            SqlParameter parm = GetPIDParameter();
            parm.Value = projectID;
            return ExcuteSqlToGetEntities("DocTask_SelectbyPIDClose", parm);
        }

        public IList<DocTaskInfo> GetDocTasksbyExe(string executor)
        {
            SqlParameter parm = GetUIDParameter(PARM_DOCTASK_EXECUTOR);
            parm.Value = executor;
            return ExcuteSqlToGetEntities("DocTask_SelectbyExe", parm);
        }

        public IList<DocTaskInfo> GetCloseDocTasks()
        {
            return ExcuteSqlToGetEntities("DocTask_SelectbyClose", null);
        }//

        public IList<DocTaskInfo> SearchDocTaskbyAll(string projectID, string publisher, string executor,string department, string projectName, string productTypeID
            , string customerID,string taskStatus,string taskName,string productTypeStep,DateTime dt1,DateTime dt2)
        {
            SqlParameter[] parms = GetSearcheParameter();
            parms[0].Value = string.IsNullOrWhiteSpace(projectID) ? null : projectID;
            parms[1].Value = string.IsNullOrWhiteSpace(publisher) ? null : publisher;
            parms[2].Value = string.IsNullOrWhiteSpace(executor) ? null : executor;
            parms[3].Value = string.IsNullOrWhiteSpace(department) ? null : department;
            parms[4].Value = string.IsNullOrWhiteSpace(projectName) ? null : projectName;
            parms[5].Value = string.IsNullOrWhiteSpace(productTypeID) ? null : productTypeID;
            parms[6].Value = string.IsNullOrWhiteSpace(customerID) ? null : customerID;
            parms[7].Value = string.IsNullOrEmpty(taskStatus) ? null : taskStatus;
            parms[8].Value = string.IsNullOrEmpty(taskName) ? null : taskName;
            parms[9].Value = string.IsNullOrEmpty(productTypeStep) ? null : productTypeStep;
            parms[10].Value = dt1;
            parms[11].Value = dt2;
            return ExcuteSqlToGetEntities("DocTask_SelectbyDetailItems", parms);
        }

        //-------------------------------------统计相关---------------------------------------------------------

        public IList<DocTask_QQInfo> CountDocTask_AllQ(string yearTime)
        {
            IList<DocTask_QQInfo> Entities = new List<DocTask_QQInfo>();
            SqlParameter parm = GetUIDParameter(PARM_DOCTASK_YEARTIME);
            parm.Value = yearTime;
            using (SqlDataReader rdr = sqlHelper.ExecuteReader(sqlHelper.ConnectionStringLocalTransaction, CommandType.StoredProcedure, "DocTask_countbyTimeAllQ", parm))
            {
                while (rdr.Read())
                {
                    DocTask_QQInfo Entity = new DocTask_QQInfo(rdr.GetValue(0).ToString(), rdr.GetDecimal(1), rdr.GetDecimal(2), rdr.GetDecimal(3), rdr.GetDecimal(4));
                    Entities.Add(Entity);
                }
            }
            return Entities;
        }

        public IList<DocTask_QInfo> CountDocTasks_Q(string quarterTime)
        {
            IList<DocTask_QInfo> Entities = new List<DocTask_QInfo>();
            SqlParameter parm = GetUIDParameter(PARM_DOCTASK_QUARTERTIME);
            parm.Value = quarterTime;
            using (SqlDataReader rdr = sqlHelper.ExecuteReader(sqlHelper.ConnectionStringLocalTransaction, CommandType.StoredProcedure, "DocTask_countbyTimeQ", parm))
            {
                while (rdr.Read())
                {
                    DocTask_QInfo Entity = new DocTask_QInfo(rdr.GetValue(0).ToString(), rdr.GetDecimal(1), rdr.GetDecimal(2), rdr.GetInt32(3), rdr.GetInt32(4), rdr.GetInt32(5), rdr.GetInt32(6), rdr.GetInt32(7), rdr.GetInt32(8));
                    Entities.Add(Entity);
                }
            }
            return Entities;
        }

        public IList<DocTask_QQInfo> CountDocTasks_Customer_AllQ(string yearTime)
        {
            IList<DocTask_QQInfo> Entities = new List<DocTask_QQInfo>();
            SqlParameter parm = GetUIDParameter(PARM_DOCTASK_YEARTIME);
            parm.Value = yearTime;
            using (SqlDataReader rdr = sqlHelper.ExecuteReader(sqlHelper.ConnectionStringLocalTransaction, CommandType.StoredProcedure, "DocTask_Customer_countbyTimeAllQ", parm))
            {
                while (rdr.Read())
                {
                    DocTask_QQInfo Entity = new DocTask_QQInfo(rdr.GetValue(0).ToString(), rdr.GetDecimal(1), rdr.GetDecimal(2), rdr.GetDecimal(3), rdr.GetDecimal(4));
                    Entities.Add(Entity);
                }
            }
            return Entities;
        }

        public IList<DocTask_QInfo> CountDocTasks_Customer_Q(string quarterTime)
        {
            IList<DocTask_QInfo> Entities = new List<DocTask_QInfo>();
            SqlParameter parm = GetUIDParameter(PARM_DOCTASK_QUARTERTIME);
            parm.Value = quarterTime;
            using (SqlDataReader rdr = sqlHelper.ExecuteReader(sqlHelper.ConnectionStringLocalTransaction, CommandType.StoredProcedure, "DocTask_Customer_countbyTimeQ", parm))
            {
                while (rdr.Read())
                {
                    DocTask_QInfo Entity = new DocTask_QInfo(rdr.GetValue(0).ToString(), rdr.GetDecimal(1), rdr.GetDecimal(2), rdr.GetInt32(3), rdr.GetInt32(4), rdr.GetInt32(5), rdr.GetInt32(6), rdr.GetInt32(7), rdr.GetInt32(8));
                    Entities.Add(Entity);
                }
            }
            return Entities;
        }

        public IList<DocTask_QInfo> CountDocTasks_DT(DateTime starttime,DateTime stoptime)
        {
            SqlParameter[] parms = GetDTParameters();
            parms[0].Value = starttime;
            parms[1].Value = stoptime;
            IList<DocTask_QInfo> Entities = new List<DocTask_QInfo>();
            using (SqlDataReader rdr = sqlHelper.ExecuteReader(sqlHelper.ConnectionStringLocalTransaction, CommandType.StoredProcedure, "DocTask_countbyDT", parms))
            {
                while (rdr.Read())
                {
                    DocTask_QInfo Entity = new DocTask_QInfo(rdr.GetValue(0).ToString(), rdr.GetDecimal(1), rdr.GetDecimal(2), rdr.GetInt32(3), rdr.GetInt32(4), rdr.GetInt32(5), rdr.GetInt32(6), rdr.GetInt32(7), rdr.GetInt32(8));
                    Entities.Add(Entity);
                }
            }
            return Entities;
        }


        public IList<DocTask_QInfo> CountDocTasks_Customer_DT(DateTime starttime, DateTime stoptime)
        {
            SqlParameter[] parms = GetDTParameters();
            parms[0].Value = starttime;
            parms[1].Value = stoptime;
            IList<DocTask_QInfo> Entities = new List<DocTask_QInfo>();
            using (SqlDataReader rdr = sqlHelper.ExecuteReader(sqlHelper.ConnectionStringLocalTransaction, CommandType.StoredProcedure, "DocTask_Customer_countbyDT", parms))
            {
                while (rdr.Read())
                {
                    DocTask_QInfo Entity = new DocTask_QInfo(rdr.GetValue(0).ToString(), rdr.GetDecimal(1), rdr.GetDecimal(2), rdr.GetInt32(3), rdr.GetInt32(4), rdr.GetInt32(5), rdr.GetInt32(6), rdr.GetInt32(7), rdr.GetInt32(8));
                    Entities.Add(Entity);
                }
            }
            return Entities;
        }


        //-------------------------------------参数相关---------------------------------------------------------
        
        private SqlParameter GetUIDParameter(string PARMNAME)
        {
            SqlParameter Parm = new SqlParameter(PARMNAME, SqlDbType.NVarChar, 20);
            return Parm;
        }

        private SqlParameter GetPIDParameter()
        {
            SqlParameter Parm = new SqlParameter(PARM_DOCTASK_PROJECTID, SqlDbType.NVarChar, 50);
            return Parm;
        }

        private SqlParameter[] GetDTParameters()
        {
            SqlParameter[] parms = new SqlParameter[]
                {
					new SqlParameter(PARM_DOCTASK_STARTTIME, SqlDbType.DateTime, 8),
					new SqlParameter(PARM_DOCTASK_STOPTIME, SqlDbType.DateTime, 8),
                };
            return parms;
        }

        private SqlParameter[] GetPIDNameParameter()
        {
            SqlParameter[] parms = new SqlParameter[]
                {
					new SqlParameter(PARM_DOCTASK_PROJECTID, SqlDbType.NVarChar, 50),
					new SqlParameter(PARM_DOCTASK_TASKNAME, SqlDbType.NVarChar, 50),
                };
            return parms;
        }

        private SqlParameter[] GetSearcheParameter()
        {
            SqlParameter[] parms = new SqlParameter[]
                {
					new SqlParameter(PARM_DOCTASK_PROJECTID, SqlDbType.NVarChar, 50),
					new SqlParameter(PARM_DOCTASK_PUBLISHER, SqlDbType.NVarChar, 20),
                    new SqlParameter(PARM_DOCTASK_EXECUTOR, SqlDbType.NVarChar, 20),
                    new SqlParameter(PARM_DOCTASK_DEPARTMENT,SqlDbType.NVarChar,50),
                    new SqlParameter(PARM_DOCTASK_PROJECTNAME, SqlDbType.NVarChar, 50),
                    new SqlParameter(PARM_DOCTASK_PRODUCTTYPEID, SqlDbType.NVarChar, 50),
                    new SqlParameter(PARM_DOCTASK_CUSTOMERID,SqlDbType.NVarChar,50),
                    new SqlParameter(PARM_DOCTASK_TASKSTATUS,SqlDbType.NVarChar,50),
                    new SqlParameter(PARM_DOCTASK_TASKNAME,SqlDbType.NVarChar,50),
                    new SqlParameter(PARM_DOCTASK_PRODUCTTYPESTEP,SqlDbType.NVarChar,50),
                    new SqlParameter(PARM_DOCTASK_STARTTIME, SqlDbType.DateTime, 8),
					new SqlParameter(PARM_DOCTASK_STOPTIME, SqlDbType.DateTime, 8)
                };
            return parms;
        }

        
        //-------------------------------------重写超类基础操作---------------------------------------------------------

        public override DocTaskInfo GetEntityRecord(SqlDataReader rdr)
        {
            DocTaskInfo pro = new DocTaskInfo(rdr.GetInt32(0), rdr.GetValue(1).ToString(), rdr.GetValue(2).ToString(), rdr.GetValue(3).ToString(), rdr.GetValue(4).ToString(), rdr.GetDateTime(5)
                , rdr.GetValue(6).ToString(), rdr.GetDateTime(7), rdr.GetDateTime(8), rdr.GetValue(9).ToString(), rdr.GetValue(10).ToString()
                , rdr.GetValue(11).ToString(), rdr.GetDateTime(12),rdr.GetInt32(13), rdr.GetValue(14).ToString(),rdr.GetValue(15).ToString()
                , rdr.IsDBNull(16)?false:rdr.GetBoolean(16));
            return pro;
        }

        public override DocTaskInfo GetEntityRecord()
        {
            DocTaskInfo pro = new DocTaskInfo();
            return pro;
        }

        public override string GetInsertsp()
        {
            return "DocTask_Insert";
        }

        public override string GetUpdatesp()
        {
            return "DocTask_Update";
        }

        public override string GetDeletesp()
        {
            return "DocTask_Delete";
        }

        public override SqlParameter GetParameter()
        {
            SqlParameter Parm = new SqlParameter(PARM_DOCTASK_DOCTASKID, SqlDbType.Int, 4);
            return Parm;
        }

        public override SqlParameter[] GetParameters()
        {
            SqlParameter[] parms = null;
            parms = new SqlParameter[]
                {
                    new SqlParameter(PARM_DOCTASK_DOCTASKID, SqlDbType.Int, 4),
					new SqlParameter(PARM_DOCTASK_PROJECTID, SqlDbType.NVarChar, 50),
					new SqlParameter(PARM_DOCTASK_TASKNAME, SqlDbType.NVarChar, 50),
                    new SqlParameter(PARM_DOCTASK_PRODUCTTYPESTEP,SqlDbType.NVarChar,50),
                    new SqlParameter(PARM_DOCTASK_PUBLISHER, SqlDbType.NVarChar, 20),
                    new SqlParameter(PARM_DOCTASK_BEGINTIME, SqlDbType.DateTime, 8),
					new SqlParameter(PARM_DOCTASK_POSTTIME, SqlDbType.DateTime, 8),
                    new SqlParameter(PARM_DOCTASK_EXECUTOR, SqlDbType.NVarChar, 20),
                    new SqlParameter(PARM_DOCTASK_DEPARTMENT, SqlDbType.NVarChar, 50),
					new SqlParameter(PARM_DOCTASK_TASKDETAIL, SqlDbType.NVarChar, 200)
                };
            return parms;
        }

        public override void SetParameters(DocTaskInfo doc, params SqlParameter[] Parms)
        {
            Parms[0].Value = doc.TaskID;
            Parms[1].Value = doc.ProjectID;
            Parms[2].Value = doc.TaskName;
            Parms[3].Value = doc.ProductTypeStep;
            Parms[4].Value = doc.Publisher;
            Parms[5].Value = doc.BeginTime;
            Parms[6].Value = doc.PostTime;
            Parms[7].Value = doc.Executor;
            Parms[8].Value = doc.Department;
            Parms[9].Value = doc.TaskDetail;
        }
    }
}
