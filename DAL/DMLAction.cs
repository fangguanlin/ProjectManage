﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DBUtility;
using IDAL;
using Model;

namespace DAL
{
    public class DMLAction : BaseRepository<DMLActionInfo>
    {
        private const string PARM_DMLACTION_USERID = "@UserID";
        private const string PARM_DMLACTION_SPNAME = "@SPName";
        private const string PARM_DMLACTION_PARAMS = "@Params";

        public void DMLActionInsert(DMLActionInfo Entity)
        {
            SqlParameter[] Parms = GetParameters();
            SetParameters(Entity, Parms);
            SqlCommand cmd = new SqlCommand();
            foreach (SqlParameter parm in Parms)
                cmd.Parameters.Add(parm);
            ExecuteCommand(cmd, GetInsertsp());
        }

        //-------------------------------------重写超类基础操作---------------------------------------------------------

        public override DMLActionInfo GetEntityRecord(SqlDataReader rdr)
        {
            return null;
        }

        public override DMLActionInfo GetEntityRecord()
        {
            return null;
        }

        public override string GetInsertsp()
        {
            return "DMLAction_Insert";
        }

        public override string GetUpdatesp()
        {
            return null;
        }

        public override string GetDeletesp()
        {
            return null;
        }

        public override SqlParameter GetParameter()
        {
            return null;
        }

        public override SqlParameter[] GetParameters()
        {
            SqlParameter[] parms = null;
            parms = new SqlParameter[]
                {
					new SqlParameter(PARM_DMLACTION_USERID, SqlDbType.NVarChar, 20),
                    new SqlParameter(PARM_DMLACTION_SPNAME, SqlDbType.NVarChar, 50),
                    new SqlParameter(PARM_DMLACTION_PARAMS, SqlDbType.NVarChar, 500)
                };
            return parms;
        }

        public override void SetParameters(DMLActionInfo dai, params SqlParameter[] Parms)
        {
            Parms[0].Value = dai.UserID;
            Parms[1].Value = dai.SPName;
            Parms[2].Value = dai.Params;
        }
    }
}
