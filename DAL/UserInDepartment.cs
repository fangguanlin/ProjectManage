﻿using IDAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class UserInDepartment : BaseRepository<UserInDepartmentInfo>, IUserInDepartment
    {
        private const string PARM_USERINDEPARTMENT_DID = "@DepartmentID";
        private const string PARM_USERINDEPARTMENT_UID = "@UserName";

        public IList<UserInDepartmentInfo> LoadEntities()
        {
            return null;
        }

        public UserInDepartmentInfo LoadEntity<T1>(T1 customerID)
        {
            return null;
        }

        public IList<UserInDepartmentInfo> UserInDepartment_GetbyDID(int departmentID)
        {
            SqlParameter parm = new SqlParameter(PARM_USERINDEPARTMENT_DID, SqlDbType.Int, 4);
            parm.Value = departmentID;
            return ExcuteSqlToGetEntities("UserInDepartment_SelectbyDID", parm);
        }       

        public IList<UserInDepartmentInfo> UserInDepartment_GetbyUID(string userName)
        {
            SqlParameter parm = new SqlParameter(PARM_USERINDEPARTMENT_UID, SqlDbType.NVarChar, 50);
            parm.Value = userName;
            return ExcuteSqlToGetEntities("UserInDepartment_SelectUID", parm);
        }

        public void UserInDepartment_Delete(UserInDepartmentInfo uidinfo)
        {
            SqlParameter[] Parms = GetParameters();
            SetParameters(uidinfo, Parms);
            SqlCommand cmd = new SqlCommand();
            foreach (SqlParameter parm in Parms)
                cmd.Parameters.Add(parm);
            ExecuteCommand(cmd, "UserInDepartment_Delete");
        }


        //-------------------------------------重写超类基础操作---------------------------------------------------------18681568015

        public override UserInDepartmentInfo GetEntityRecord(SqlDataReader rdr)
        {
            UserInDepartmentInfo pro = new UserInDepartmentInfo(rdr.GetInt32(0), rdr.GetValue(1).ToString());
            return pro;
        }

        public override UserInDepartmentInfo GetEntityRecord()
        {
            UserInDepartmentInfo pro = new UserInDepartmentInfo();
            return pro;
        }

        public override string GetInsertsp()
        {
            return "UserInDepartment_Insert";
        }

        public override string GetUpdatesp()
        {
            return null;
        }

        public override string GetDeletesp()
        {
            return null;
        }

        public override SqlParameter GetParameter()
        {
            SqlParameter Parm = new SqlParameter(PARM_USERINDEPARTMENT_DID, SqlDbType.Int, 4);
            return Parm;
        }

        public override SqlParameter[] GetParameters()
        {
            SqlParameter[] parms = null;
            parms = new SqlParameter[]
                {
					new SqlParameter(PARM_USERINDEPARTMENT_DID, SqlDbType.Int, 4),
                    new SqlParameter(PARM_USERINDEPARTMENT_UID, SqlDbType.NVarChar, 50)
                };
            return parms;
        }

        public override void SetParameters(UserInDepartmentInfo pro, params SqlParameter[] Parms)
        {
            Parms[0].Value = pro.DepartmentID;
            Parms[1].Value = pro.UserName;
        }
    }
}
