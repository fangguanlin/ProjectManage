﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using IDAL;
using Model;

namespace DAL
{
    public class ProductType : BaseRepository<ProductTypeInfo>, IProductType
    {
        private const string PARM_PRODUCTTYPE_PRODUCTTYPEID = "@ProductTypeID";
        private const string PARM_PRODUCTTYPE_PRODUCTTYPENAME = "@ProductTypeName";
        private const string PARM_PRODUCTTYPE_PROJECTID = "@ProjectID";
        private const string PARM_PRODUCTTYPE_CUSTOMERID = "@CustomerID";
        private const string PARM_PRODUCTTYPE_REMARK = "@Remark";

        public ProductTypeInfo LoadEntity<T1>(T1 productTypeID)
        {
            SqlParameter parm = GetParameter();
            //Bind the parameter
            parm.Value = productTypeID;
            return ExcuteSqlToGetEntity("ProductType_Select", parm);
        }

        public IList<ProductTypeInfo> LoadEntities()
        {
            return null;
        }

        public IList<ProductTypeInfo> GetProductTypesbyProID(string projectID)
        {
            SqlParameter parm = GetProIDParameter();
            //Bind the parameter
            parm.Value = projectID;
            return ExcuteSqlToGetEntities("ProductType_SelectbyProID", parm);
        }

        private SqlParameter GetProIDParameter()
        {
            SqlParameter Parm = new SqlParameter(PARM_PRODUCTTYPE_PROJECTID, SqlDbType.NVarChar, 50);
            return Parm;
        }


        //-------------------------------------重写超类基础操作---------------------------------------------------------

        public override ProductTypeInfo GetEntityRecord(SqlDataReader rdr)
        {
            ProductTypeInfo pro = new ProductTypeInfo(rdr.GetValue(0).ToString(), rdr.GetValue(1).ToString(), rdr.GetValue(2).ToString(), rdr.GetValue(3).ToString()
                , rdr.GetValue(4).ToString(),rdr.GetValue(5).ToString());
            return pro;
        }

        public override ProductTypeInfo GetEntityRecord()
        {
            ProductTypeInfo pro = new ProductTypeInfo();
            return pro;
        }

        public override string GetInsertsp()
        {
            return "ProductType_Insert";
        }

        public override string GetUpdatesp()
        {
            return "ProductType_Update";
        }

        public override string GetDeletesp()
        {
            return "ProductType_Delete";
        }

        public override SqlParameter GetParameter()
        {
            SqlParameter Parm = new SqlParameter(PARM_PRODUCTTYPE_PRODUCTTYPEID, SqlDbType.NVarChar, 50);
            return Parm;
        }

        public override SqlParameter[] GetParameters()
        {
            SqlParameter[] parms = null;
            parms = new SqlParameter[]
                {
					new SqlParameter(PARM_PRODUCTTYPE_PRODUCTTYPEID, SqlDbType.NVarChar, 50),
                    new SqlParameter(PARM_PRODUCTTYPE_PRODUCTTYPENAME, SqlDbType.NVarChar, 50),
					new SqlParameter(PARM_PRODUCTTYPE_PROJECTID, SqlDbType.NVarChar, 50),
                    new SqlParameter(PARM_PRODUCTTYPE_CUSTOMERID, SqlDbType.NVarChar, 50),
					new SqlParameter(PARM_PRODUCTTYPE_REMARK, SqlDbType.NVarChar, 200),
                };
            return parms;
        }

        public override void SetParameters(ProductTypeInfo pro, params SqlParameter[] Parms)
        {
            Parms[0].Value = pro.ProductTypeID;
            Parms[1].Value = pro.ProductTypeName;
            Parms[2].Value = pro.ProjectID;
            Parms[3].Value = pro.CustomerID;
            Parms[4].Value = pro.Remark;
        }
    }
}
