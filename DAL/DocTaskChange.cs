﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using IDAL;
using Model;

namespace DAL
{
    public class DocTaskChange : BaseRepository<DocTaskChangeInfo>, IDocTaskChange
    {
        private const string PARM_DOCTASKCHANGE_DOCTASKID = "@TaskID";
        private const string PARM_DOCTASKCHANGE_USERID = "@UserID";
        private const string PARM_DOCTASKCHANGE_CHANGEDETAIL = "@ChangeDetail";        

        public IList<DocTaskChangeInfo> LoadEntities()
        {
            return null;
        }

        /// <summary>
        /// Get an individual category based on a provided id
        /// </summary>
        /// <param name="categoryId">Category id</param>
        /// <returns>Details about the Category</returns>
        public DocTaskChangeInfo LoadEntity<T1>(T1 taskID)
        {
            return null;
        }

        public IList<DocTaskChangeInfo> GetDocTaskChangesbyID(int taskID)
        {
            SqlParameter parm = GetParameter();
            //Bind the parameter
            parm.Value = taskID;
            return ExcuteSqlToGetEntities("DocTaskChange_SelectbyID", parm);
        }

        //-------------------------------------参数相关---------------------------------------------------------

        

        //-------------------------------------重写超类基础操作---------------------------------------------------------

        public override DocTaskChangeInfo GetEntityRecord(SqlDataReader rdr)
        {
            DocTaskChangeInfo pro = new DocTaskChangeInfo(rdr.GetInt32(0), rdr.GetDateTime(1), rdr.GetValue(2).ToString(),rdr.GetValue(3).ToString());
            return pro;
        }

        public override DocTaskChangeInfo GetEntityRecord()
        {
            DocTaskChangeInfo pro = new DocTaskChangeInfo();
            return pro;
        }

        public override string GetInsertsp()
        {
            return "DocTaskChange_Insert";
        }

        public override string GetUpdatesp()
        {
            return null;
        }

        public override string GetDeletesp()
        {
            return null;
        }

        public override SqlParameter GetParameter()
        {
            SqlParameter Parm = new SqlParameter(PARM_DOCTASKCHANGE_DOCTASKID, SqlDbType.Int, 4);
            return Parm;
        }

        public override SqlParameter[] GetParameters()
        {
            SqlParameter[] parms = null;
            parms = new SqlParameter[]
                {
					new SqlParameter(PARM_DOCTASKCHANGE_DOCTASKID, SqlDbType.Int, 4),
					new SqlParameter(PARM_DOCTASKCHANGE_USERID, SqlDbType.NVarChar, 20),
                    new SqlParameter(PARM_DOCTASKCHANGE_CHANGEDETAIL, SqlDbType.NVarChar, 200),
                };
            return parms;
        }

        public override void SetParameters(DocTaskChangeInfo doc, params SqlParameter[] Parms)
        {
            Parms[0].Value = doc.TaskID;
            Parms[1].Value = doc.UserID;
            Parms[2].Value = doc.ChangeDetail;
        }
    }
}
