﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using IDAL;
using Model;

namespace DAL
{
    public class DocTaskCheck : BaseRepository<DocTaskCheckInfo>, IDocTaskCheck
    {
        private const string PARM_DOCTASKCHECK_DOCTASKID = "@TaskID";
        private const string PARM_DOCTASKCHECK_USERID = "@UserID";
        private const string PARM_DOCTASKCHECK_ENDTIME = "@EndTime";
        private const string PARM_DOCTASKCHECK_CLOSESTATUS = "@CloseStatus";

        public IList<DocTaskCheckInfo> LoadEntities()
        {
            return null;
        }

        /// <summary>
        /// Get an individual category based on a provided id
        /// </summary>
        /// <param name="categoryId">Category id</param>
        /// <returns>Details about the Category</returns>
        public DocTaskCheckInfo LoadEntity<T1>(T1 taskID)
        {
            SqlParameter parm = GetParameter();
            //Bind the parameter
            parm.Value = taskID;
            return ExcuteSqlToGetEntity("DocTaskCheck_SelectbyID", parm);
        }

        public void Insert_Unexpect(DocTaskCheckInfo doc)
        {
            InsertOrUpdate(doc, "DocTaskCheck_Insert_Unexpect");
        }

        //-------------------------------------参数相关---------------------------------------------------------



        //-------------------------------------重写超类基础操作---------------------------------------------------------

        public override DocTaskCheckInfo GetEntityRecord(SqlDataReader rdr)
        {
            DocTaskCheckInfo pro = new DocTaskCheckInfo(rdr.GetInt32(0), rdr.GetDateTime(1),rdr.GetValue(2).ToString(), rdr.GetDateTime(3), rdr.GetValue(4).ToString());
            return pro;
        }

        public override DocTaskCheckInfo GetEntityRecord()
        {
            DocTaskCheckInfo pro = new DocTaskCheckInfo();
            return pro;
        }

        public override string GetInsertsp()
        {
            return "DocTaskCheck_Insert";
        }

        public override string GetUpdatesp()
        {
            return null;
        }

        public override string GetDeletesp()
        {
            return null;
        }

        public override SqlParameter GetParameter()
        {
            SqlParameter Parm = new SqlParameter(PARM_DOCTASKCHECK_DOCTASKID, SqlDbType.Int, 4);
            return Parm;
        }

        public override SqlParameter[] GetParameters()
        {
            SqlParameter[] parms = null;
            parms = new SqlParameter[]
                {
					new SqlParameter(PARM_DOCTASKCHECK_DOCTASKID, SqlDbType.Int, 4),
					new SqlParameter(PARM_DOCTASKCHECK_USERID, SqlDbType.NVarChar, 20),
                    new SqlParameter(PARM_DOCTASKCHECK_ENDTIME, SqlDbType.DateTime, 8),
                    new SqlParameter(PARM_DOCTASKCHECK_CLOSESTATUS, SqlDbType.NVarChar, 200),
                };
            return parms;
        }

        public override void SetParameters(DocTaskCheckInfo doc, params SqlParameter[] Parms)
        {
            Parms[0].Value = doc.TaskID;
            Parms[1].Value = doc.UserID;
            Parms[2].Value = doc.EndTime;
            Parms[3].Value = doc.CloseStatus;
        }
    }
}
