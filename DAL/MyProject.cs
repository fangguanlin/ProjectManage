﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DBUtility;
using IDAL;
using Model;

namespace DAL
{
    public class MyProject : BaseRepository<MyProjectInfo>, IMyProject
    {
        private const string PARM_MYPROJECT_ID = "@ProjectID";
        private const string PARM_MYPROJECT_USERID = "@UserID";

        public IList<MyProjectInfo> LoadEntities()
        {
            return null;
        }

        public MyProjectInfo LoadEntity<T1>(T1 myProjectInfo)
        {
            return null;
        }

        public MyProjectInfo MyProject_GetbyID(MyProjectInfo mpi)
        {
            SqlParameter[] parms = GetParameters();
            SetParameters(mpi, parms);
            return ExcuteSqlToGetEntity("MyProject_SelectbyID", parms);
        }

        public void MyProjectDelete(MyProjectInfo mpi)
        {
            SqlParameter[] parms = GetParameters();
            SetParameters(mpi,parms);
            SqlCommand cmd = new SqlCommand();
            foreach (SqlParameter parm in parms)
                cmd.Parameters.Add(parm);
            ExecuteCommand(cmd, "MyProject_Delete");
        }


        //-------------------------------------重写超类基础操作---------------------------------------------------------

        public override MyProjectInfo GetEntityRecord(SqlDataReader rdr)
        {
            MyProjectInfo pro = new MyProjectInfo(rdr.GetValue(0).ToString(), rdr.GetValue(1).ToString());
            return pro;
        }

        public override MyProjectInfo GetEntityRecord()
        {
            MyProjectInfo pro = new MyProjectInfo();
            return pro;
        }

        public override string GetInsertsp()
        {
            return "MyProject_Insert";
        }

        public override string GetUpdatesp()
        {
            return null;
        }

        public override string GetDeletesp()
        {
            return null;
        }

        public override SqlParameter GetParameter()
        {
            SqlParameter Parm = new SqlParameter(PARM_MYPROJECT_ID, SqlDbType.NVarChar, 50);
            return Parm;
        }

        public override SqlParameter[] GetParameters()
        {
            SqlParameter[] parms = null;
            parms = new SqlParameter[]
                {
					new SqlParameter(PARM_MYPROJECT_ID, SqlDbType.NVarChar, 50),
                    new SqlParameter(PARM_MYPROJECT_USERID, SqlDbType.NVarChar, 20)
                };
            return parms;
        }

        public override void SetParameters(MyProjectInfo pro, params SqlParameter[] Parms)
        {
            Parms[0].Value = pro.ProjectID;
            Parms[1].Value = pro.UserID;
        }
    }
}
