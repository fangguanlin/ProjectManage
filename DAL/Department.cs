﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DBUtility;
using IDAL;
using Model;

namespace DAL
{
    public class Department : BaseRepository<DepartmentInfo>, IDepartment
    {
        private const string PARM_DEPARTMENT_ID = "@DepartmentID";
        private const string PARM_DEPARTMENT_NAME = "@DepartmentName";

        public IList<DepartmentInfo> LoadEntities()
        {
            return ExcuteSqlToGetEntities("Department_SelectAll");
        }

        public DepartmentInfo LoadEntity<T1>(T1 departmentID)
        {
            SqlParameter parm = GetParameter();
            parm.Value = departmentID;
            return ExcuteSqlToGetEntity("Department_Select", parm);
        }

        public DepartmentInfo Department_GetbyName(string departmentName)
        {
            SqlParameter parm = new SqlParameter(PARM_DEPARTMENT_NAME, SqlDbType.NVarChar, 50);
            parm.Value = departmentName;
            return ExcuteSqlToGetEntity("Department_SelectbyName", parm);
        }


        //-------------------------------------重写超类基础操作---------------------------------------------------------

        public override DepartmentInfo GetEntityRecord(SqlDataReader rdr)
        {
            DepartmentInfo pro = new DepartmentInfo(rdr.GetInt32(0), rdr.GetValue(1).ToString());
            return pro;
        }

        public override DepartmentInfo GetEntityRecord()
        {
            DepartmentInfo pro = new DepartmentInfo();
            return pro;
        }

        public override string GetInsertsp()
        {
            return "Department_Insert";
        }

        public override string GetUpdatesp()
        {
            return null;
        }

        public override string GetDeletesp()
        {
            return "Department_Delete";
        }

        public override SqlParameter GetParameter()
        {
            SqlParameter Parm = new SqlParameter(PARM_DEPARTMENT_ID, SqlDbType.Int, 4);
            return Parm;
        }

        public override SqlParameter[] GetParameters()
        {
            SqlParameter[] parms = null;
            parms = new SqlParameter[]
                {
					new SqlParameter(PARM_DEPARTMENT_ID, SqlDbType.Int, 4),
                    new SqlParameter(PARM_DEPARTMENT_NAME, SqlDbType.NVarChar, 50)
                };
            return parms;
        }

        public override void SetParameters(DepartmentInfo pro, params SqlParameter[] Parms)
        {
            Parms[0].Value = pro.DepartmentID;
            Parms[1].Value = pro.DepartmentName;
        }

    }
}
