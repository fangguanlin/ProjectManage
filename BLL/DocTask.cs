﻿using System;
using System.Collections.Generic;
using IDAL;
using Model;

namespace BLL
{
    public class DocTask : BaseService<DocTaskInfo>
    {
        private static readonly IDocTask dal = DALFactory.DataAccess.CreateDocTask();

        public override void SetCurrentRepository()
        {
            CurrentRepository = dal;
        }

        public DocTaskInfo GetDocTaskbyPIDName(string projectID, string taskName)
        {
            return dal.GetDocTaskbyPIDName(projectID, taskName);
        }

        public IList<DocTaskInfo> GetDocTasksbyPID(string projectID)
        {
            return dal.GetDocTasksbyPID(projectID);
        }

        public IList<DocTaskInfo> GetDocTasksbyPIDClose(string projectID)
        {
            return dal.GetDocTasksbyPIDClose(projectID);
        }

        public IList<DocTaskInfo> GetDocTasksbyExe(string executor)
        {
            return dal.GetDocTasksbyExe(executor);
        }

        public IList<DocTaskInfo> GetCloseDocTasks()
        {
            return dal.GetCloseDocTasks();
        }//

        public IList<DocTaskInfo> SearchDocTaskbyAll(string projectID, string publisher, string executor, string department, string projectName, string productTypeID
            , string customerID, string taskStatus, string taskName, string productTypeStep,DateTime dt1,DateTime dt2)
        {
            return dal.SearchDocTaskbyAll(projectID, publisher, executor, department, projectName, productTypeID, customerID, taskStatus, taskName, productTypeStep, dt1, dt2);
        }

        public IList<DocTask_QQInfo> CountDocTask_AllQ(string yearTime)
        {
            return dal.CountDocTask_AllQ(yearTime);
        }

        public IList<DocTask_QInfo> CountDocTasks_Q(string quarterTime)
        {
            return dal.CountDocTasks_Q(quarterTime);
        }

        public IList<DocTask_QQInfo> CountDocTasks_Customer_AllQ(string yearTime)
        {
            return dal.CountDocTasks_Customer_AllQ(yearTime);
        }

        public IList<DocTask_QInfo> CountDocTasks_Customer_Q(string quarterTime)
        {
            return dal.CountDocTasks_Customer_Q(quarterTime);
        }

        public IList<DocTask_QInfo> CountDocTasks_DT(DateTime starttime, DateTime stoptime)
        {
            return dal.CountDocTasks_DT(starttime, stoptime);
        }

        public IList<DocTask_QInfo> CountDocTasks_Customer_DT(DateTime starttime, DateTime stoptime)
        {
            return dal.CountDocTasks_Customer_DT(starttime, stoptime);
        }
    }
}
