﻿using System.Collections.Generic;
using IDAL;
using Model;

namespace BLL
{
    public class ProductType : BaseService<ProductTypeInfo>
    {
        private static readonly IProductType dal = DALFactory.DataAccess.CreateProductType();

        public override void SetCurrentRepository()
        {
            CurrentRepository = dal;
        }

        public IList<ProductTypeInfo> GetProductTypesbyProID(string projectID)
        {
            return dal.GetProductTypesbyProID(projectID);
        }
    }
}
