﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using IDAL;
using Model;

namespace BLL
{
    public class DocTaskCheck : BaseService<DocTaskCheckInfo>
    {
        private static readonly IDocTaskCheck dal = DALFactory.DataAccess.CreateDocTaskCheck();

        public override void SetCurrentRepository()
        {
            CurrentRepository = dal;
        }

        public void Insert_Unexpect(DocTaskCheckInfo doc)
        {
            dal.Insert_Unexpect(doc);
        }
    }
}
