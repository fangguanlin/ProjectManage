﻿using System;
using System.Data;
using System.Collections.Generic;
using IDAL;
using Model;

namespace BLL
{
    public class UserInDepartment : BaseService<UserInDepartmentInfo>
    {
        private static readonly IUserInDepartment dal = DALFactory.DataAccess.CreateUserInDepartment();

        public override void SetCurrentRepository()
        {
            CurrentRepository = dal;
        }

        public void UserInDepartment_Delete(UserInDepartmentInfo uidinfo)
        {
            dal.UserInDepartment_Delete(uidinfo);
        }

        public IList<UserInDepartmentInfo> UserInDepartment_GetbyDID(int departmentID)
        {
            return dal.UserInDepartment_GetbyDID(departmentID);
        }

        public IList<UserInDepartmentInfo> UserInDepartment_GetbyUID(string userName)
        {
            return dal.UserInDepartment_GetbyUID(userName);
        }
    }
}
