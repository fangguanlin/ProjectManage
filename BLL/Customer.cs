﻿using System.Data;
using System.Collections.Generic;
using IDAL;
using Model;

namespace BLL
{
    public class Customer : BaseService<CustomerInfo>
    {
        private static readonly ICustomer dal = DALFactory.DataAccess.CreateCustomer();

        public override void SetCurrentRepository()
        {
            CurrentRepository = dal;
        }
        public CustomerInfo Customer_GetbyName(string customerName)
        {
            return dal.Customer_GetbyName(customerName);
        }
    }
}
