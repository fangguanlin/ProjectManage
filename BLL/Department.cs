﻿using System.Data;
using System.Collections.Generic;
using IDAL;
using Model;

namespace BLL
{
    public class Department : BaseService<DepartmentInfo>
    {
        private static readonly IDepartment dal = DALFactory.DataAccess.CreateDepartment();

        public override void SetCurrentRepository()
        {
            CurrentRepository = dal;
        }

        public DepartmentInfo Department_GetbyName(string departmentName)
        {
            return dal.Department_GetbyName(departmentName);
        }
    }
}
