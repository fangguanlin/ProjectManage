﻿using System.Collections.Generic;

namespace BLL
{
    public abstract class BaseService<T> where T : class, new()
    {
        public IDAL.IBaseRepository<T> CurrentRepository { get; set; }

        //基类的构造函数
        public BaseService()
        {
            SetCurrentRepository();  //构造函数里面去调用了，此设置当前仓储的抽象方法
        }

        //约束
        public abstract void SetCurrentRepository();  //子类必须实现

        public void Insert(T entity,string userID)
        {
            //调用T对应的仓储来做添加工作
            CurrentRepository.Insert(entity,userID);               
        }

        public void Update(T entity, string userID)
        {
            CurrentRepository.Update(entity,userID);
        }

        public void Delete<T1>(T1 Id, string userID)
        {
            CurrentRepository.Delete(Id, userID);
        }

        //T1 may be <int> or <string> 

        public T LoadEntity<T1>(T1 Id)
        {
            return CurrentRepository.LoadEntity(Id);
        }

        public IList<T> LoadEntities()
        {
            return CurrentRepository.LoadEntities();
        }
    }
}
