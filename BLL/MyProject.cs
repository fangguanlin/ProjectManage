﻿using System.Data;
using System.Collections.Generic;
using IDAL;
using Model;

namespace BLL
{
    public class MyProject : BaseService<MyProjectInfo>
    {
        private static readonly IMyProject dal = DALFactory.DataAccess.CreateMyProject();

        public override void SetCurrentRepository()
        {
            CurrentRepository = dal;
        }

        public void MyProjectDelete(MyProjectInfo mpi)
        {
            dal.MyProjectDelete(mpi);
        }

        public MyProjectInfo MyProject_GetbyID(MyProjectInfo mpi)
        {
            return dal.MyProject_GetbyID(mpi);
        }
    }
}
