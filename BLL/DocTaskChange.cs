﻿using System.Collections.Generic;
using IDAL;
using Model;

namespace BLL
{
    public class DocTaskChange : BaseService<DocTaskChangeInfo>
    {
        private static readonly IDocTaskChange dal = DALFactory.DataAccess.CreateDocTaskChange();

        public override void SetCurrentRepository()
        {
            CurrentRepository = dal;
        }

        public IList<DocTaskChangeInfo> GetDocTaskChangesbyID(int taskID)
        {
            return dal.GetDocTaskChangesbyID(taskID);
        }
    }
}
