﻿using System.Data;
using System.Collections.Generic;
using IDAL;
using Model;

namespace BLL
{
    public class Project : BaseService<ProjectInfo>
    {
        private static readonly IProject dal = DALFactory.DataAccess.CreateProject();

        public override void SetCurrentRepository()
        {
            CurrentRepository = dal;
        }

        public IList<ProjectInfo> Project_GetbyClose()
        {
            return dal.Project_GetbyClose();
        }
        public bool ProjectItems_Insert(ProjectInfo proi, DataTable itemdt)
        {
            return dal.ProjectItems_Insert(proi, itemdt);
        }

        public void Project_SetClose(string projectID)
        {
            dal.Project_SetClose(projectID);
        }

        public IList<ProjectInfo> Project_GetMyProject(string userID)
        {
            return dal.Project_GetMyProject(userID);
        }

        public IList<ProjectInfo> Project_GetbySearch(string searchFactor)
        {
            return dal.Project_GetbySearch(searchFactor);
        }
    }
}
