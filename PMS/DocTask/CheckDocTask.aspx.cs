﻿using System;
using System.Web.UI;
using BLL;
using Model;

namespace PMS
{
    public partial class CheckDocTask : System.Web.UI.Page
    {
        private DocTaskCheck doctc;
        private int taskID
        {
            get { return int.Parse(ViewState["taskID"].ToString()); }
            set { ViewState["taskID"] = value; }
        }

        public CheckDocTask()
        {
            doctc = new DocTaskCheck();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["TaskID"] != null)
                {
                    taskID = int.Parse(Request.QueryString["TaskID"]);
                    //Session["TaskID"] = taskID;
                    BindUserControl();
                }
            }
            //taskID = int.Parse(Session["TaskID"].ToString());
        }

        protected void BindUserControl()
        {
            DocTaskDetail uc = (DocTaskDetail)LoadControl("~/UserControl/DocTaskDetail.ascx");
            uc.taskID = taskID;
            this.PlaceHolder1.Controls.Add(uc);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (IsValidtoInsert())
            {
                DocTaskCheckInfo doctci = new DocTaskCheckInfo(taskID, DateTime.Now, User.Identity.Name, DateTime.Parse(this.TextBox1.Text.Trim()), this.TextBox2.Text.Trim());
                try
                {
                    doctc.Insert(doctci, Page.User.Identity.Name);
                    string str = "任务完成情况确认成功！";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
                    BindUserControl();
                }
                catch
                {
                    string str = "任务完成确认失败，请联系相关人员！";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
                }
            }
        }


        protected void Button3_Click(object sender, EventArgs e)
        {
            if (IsValidtoInsert())
            {
                DocTaskCheckInfo doctci = new DocTaskCheckInfo(taskID, DateTime.Now, User.Identity.Name, DateTime.Now, this.TextBox2.Text.Trim());
                try
                {
                    doctc.Insert_Unexpect(doctci);
                    string str = "任务完成情况确认成功！";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
                    BindUserControl();
                }
                catch
                {
                    string str = "任务完成确认失败，请联系相关人员！";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
                }
            }
        }

        private bool IsValidtoInsert()
        {
            DocTask doc = new DocTask();
            DocTaskInfo docInfo = doc.LoadEntity(taskID);
            if (docInfo.TaskStatus != "OPEN")
            {
                string str = "任务已经关闭，无法做此操作!";
                ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
                return false;
            }
            else if (User.Identity.Name != docInfo.Publisher)
            {
                string str = "您无权做此操作，只有任务发布人能做相应任务的完成确认!";
                ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
                return false;
            }
            return true;
        }
    }
}