﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DocTaskAdd.aspx.cs" Inherits="PMS.DocTaskAdd" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<script type="text/javascript" src="../Scripts/my97datepicker/WdatePicker.js"></script>--%>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
<div style=" text-align:center;">
        <br />
        <br />
        <br />
        <br />
        <br />
        <table style="width: 60%; border:solid 1px black;  margin:0 auto;">
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    项目编号：</td>
                <td>
                    <asp:TextBox ID="TextBox1" runat="server" TabIndex="1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="TextBox1" ErrorMessage="*" ValidationGroup="VGroup1" 
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    任务名称：</td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="TextBox2" ErrorMessage="*" ValidationGroup="VGroup1" 
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    开始时间：</td>
                <td>
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="TextBox3_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="TextBox3">
                    </ajaxToolkit:CalendarExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="TextBox3" ErrorMessage="*" ValidationGroup="VGroup1" 
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    要求完成时间：</td>
                <td>
                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="TextBox4_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="TextBox4">
                    </ajaxToolkit:CalendarExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="TextBox4" ErrorMessage="*" ValidationGroup="VGroup1" 
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    实施人：</td>
                <td>
                    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                        ControlToValidate="TextBox5" ErrorMessage="*" ValidationGroup="VGroup1" 
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    实施部门：</td>
                <td>
                    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                        ControlToValidate="TextBox6" ErrorMessage="*" ValidationGroup="VGroup1" 
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    任务详细：</td>
                <td>
                    <asp:TextBox ID="TextBox8" runat="server" Height="62px" TextMode="MultiLine"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="Button1" runat="server" Text="提 交" ValidationGroup="VGroup1" 
                        onclick="Button1_Click" TabIndex="2" />
                </td>
            </tr>
            </table>        
        <br />
        <br />
        <div style=" text-align:center;">
        <br />
        <table style="width: 60%; border:solid 1px black;  margin:0 auto;">
            <tr>
                <td colspan="3">
                    查询任务是否发布</td>
            </tr>
            <tr>
                <td colspan="3">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    项目编号：<asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
                </td>
                <td>
                    任务名称：<asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="Button2" runat="server" onclick="Button2_Click" Text="查 询" 
                        ValidationGroup="VGroup2" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:GridView ID="GridView2" runat="server" Width="100%" AutoGenerateColumns="False" 
                        PageSize="20" CssClass="gridview_m" >
                        <Columns>
                            <asp:BoundField DataField="ProjectID" HeaderText="项目编号" />
                            <asp:BoundField DataField="TaskName" HeaderText="任务名称" />
                            <asp:BoundField DataField="RecordTime" HeaderText="录入时间" 
                                DataFormatString="{0:d}" />
                            <asp:BoundField DataField="Publisher" HeaderText="发布人" />
                            <asp:BoundField DataField="BeginTime" HeaderText="开始时间" 
                                DataFormatString="{0:d}" />
                            <asp:BoundField DataField="PostTime" HeaderText="要求完成时间" 
                                DataFormatString="{0:d}" />
                            <asp:BoundField DataField="Executor" HeaderText="实施人" />
                            <asp:BoundField DataField="Department" HeaderText="实施部门" />
                            <asp:BoundField DataField="TaskDetail" HeaderText="任务详细" />
                            <asp:BoundField DataField="TaskStatus" HeaderText="任务状态" />
                        </Columns>
                    </asp:GridView>
            <br />
                </td>
            </tr>
        </table>        
        <br />
        <br />
        <br />
            <br />
        <br />
        <br />
    </div>

        <br />
    </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
