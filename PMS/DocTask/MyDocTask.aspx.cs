﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using PMS.App_Code;

namespace PMS
{
    public partial class MyDocTask : System.Web.UI.Page
    {
        private DocTask doc;

        public MyDocTask()
        {
            doc = new DocTask();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Menu m = (Menu)Master.FindControl("NavigationMenu");  
            BindDocTaskforExe();
        }

        protected void BindDocTaskforExe()
        {
            this.GridView1.DataSource = doc.GetDocTasksbyExe(User.Identity.Name);
            this.GridView1.DataBind();
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (DateTime.Parse(GridView1.DataKeys[i].Values[1].ToString()) == DateTime.Parse("1900-01-01 00:00:00"))
                {
                    GridView1.Rows[i].Cells[8].Text = "";
                    GridView1.Rows[i].Cells[14].Text = "";
                }
                if (GridView1.DataKeys[i].Values[2].ToString() != "OPEN" || GridView1.DataKeys[i].Values[3].ToString() != User.Identity.Name)
                {
                    GridView1.Rows[i].Cells[15].Enabled = false;
                    GridView1.Rows[i].Cells[16].Enabled = false;
                }
            }
        }

        protected void GridView1_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            this.GridView1.PageIndex = e.NewPageIndex;
            BindDocTaskforExe();
        }

        protected void GridView1_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    ((LinkButton)e.Row.Cells[17].Controls[0]).Attributes.Add("onclick", "javascript:return confirm('你确认要删除!')");
                }
                if (bool.Parse(GridView1.DataKeys[e.Row.RowIndex].Values[4].ToString()))
                {
                    e.Row.Cells[1].ForeColor = CommData.IMPORTANT_COLOR;
                }
                if (GridView1.DataKeys[e.Row.RowIndex].Values[2].ToString() == "OPEN")
                {
                    e.Row.Cells[9].ForeColor = CommData.OPEN_COLOR;
                }
                if (GridView1.DataKeys[e.Row.RowIndex].Values[2].ToString() == "DELAYCLOSED")
                {
                    e.Row.Cells[9].ForeColor = CommData.DELAYCLOSED_COLOR;
                }
            }
        }

        protected void GridView1_RowDeleting(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            if (User.Identity.Name == GridView1.DataKeys[e.RowIndex].Values[3].ToString())
            {
                int taskID = int.Parse(GridView1.DataKeys[e.RowIndex].Values[0].ToString());
                doc.Delete(taskID, Page.User.Identity.Name);
                BindDocTaskforExe();
            }
            else
            {
                string str = "无权删除，只有相应的项目发布人能做此操作！";
                ClientScript.RegisterStartupScript(ClientScript.GetType(), "", "<script>alert('" + str + "');</script>");
            }
        }
    }
}