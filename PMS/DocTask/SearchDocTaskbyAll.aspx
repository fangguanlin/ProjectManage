﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SearchDocTaskbyAll.aspx.cs" Inherits="PMS.SearchDocTaskbyAll" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1 {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div style="width: 80%; border: solid 1px black; margin: 0 auto;">
            <table class="style1">
                <tr>
                    <td>项目编号：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    </td>
                    <td>项目名称：<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                    </td>
                    <td>产品代码：<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>客户名称：<asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                    </td>
                    <td>发 布 人 ：<asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                    </td>
                    <td>实 施 人 ：<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                    </td>
                    <td align="center">&nbsp;</td>
                </tr>
                <tr>
                    <td>实施部门：<asp:DropDownList ID="DropDownList2" runat="server">
                    </asp:DropDownList>
                    </td>
                    <td>任务名称：<asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                    </td>
                    <td>产品阶段：<asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                    </td>
                    <td align="center">&nbsp;</td>
                </tr>
                <tr>
                    <td>任务状态：<asp:DropDownList ID="DropDownList1" runat="server">
                    </asp:DropDownList>
                    </td>
                    <td>起始时间：<asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="TextBox9_CalendarExtender" runat="server" Enabled="True" TargetControlID="TextBox9">
                        </ajaxToolkit:CalendarExtender>
                    </td>
                    <td>
终止时间：<asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="TextBox10_CalendarExtender" runat="server" Enabled="True" TargetControlID="TextBox10">
                        </ajaxToolkit:CalendarExtender>
                    </td>
                    <td align="center">
                        <asp:Button ID="Button1" runat="server" Text="搜  索" OnClick="Button1_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        搜索结果：<br />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True"
            AutoGenerateColumns="False" PageSize="20" Width="100%" CssClass="gridview_m"
            OnPageIndexChanging="GridView1_PageIndexChanging"
            DataKeyNames="TaskID,EndTime,TaskStatus,Publisher,IsImportant"
            OnRowDataBound="GridView1_RowDataBound"
            OnRowDeleting="GridView1_RowDeleting" ShowFooter="True">
            <Columns>
                <asp:TemplateField HeaderText="序号">
                    <FooterTemplate>
                        共<asp:Label ID="Label1" runat="server"></asp:Label>
                        记录
                    </FooterTemplate>
                    <ItemTemplate>
                        <%# Container.DataItemIndex +1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ProjectID" HeaderText="项目编号" />
                <asp:BoundField DataField="ProjectName" HeaderText="项目名称" />
                <asp:BoundField DataField="ProductTypeStep" HeaderText="产品阶段" />
                <asp:HyperLinkField DataNavigateUrlFields="TaskID"
                    DataNavigateUrlFormatString="~/DocTask/ViewDocTaskDetails.aspx?TaskID={0}"
                    DataTextField="TaskName" HeaderText="任务名称"
                    NavigateUrl="~/DocTask/ViewDocTaskDetails.aspx" />
                <asp:BoundField DataField="TaskDetail" HeaderText="任务详细" />
                <asp:BoundField DataField="Executor" HeaderText="实施人" />
                <asp:BoundField DataField="Department" HeaderText="实施部门" />
                <asp:BoundField DataField="PostTime" HeaderText="要求完成时间"
                    DataFormatString="{0:d}" />
                <asp:BoundField DataField="EndTime" HeaderText="实际完成时间"
                    DataFormatString="{0:d}" />
                <asp:BoundField DataField="TaskStatus" HeaderText="任务状态" />
                <asp:BoundField DataField="CloseStatus" HeaderText="完成情况&延期原因" />
                <asp:BoundField DataField="Publisher" HeaderText="发布人" />
                <asp:BoundField DataField="RecordTime" HeaderText="录入时间"
                    DataFormatString="{0:d}" Visible="False" />
                <asp:BoundField DataField="BeginTime" HeaderText="开始时间"
                    DataFormatString="{0:d}" Visible="False" />
                <asp:BoundField DataField="ConsumeDays" HeaderText="消耗工时" Visible="False" />
                <asp:HyperLinkField DataNavigateUrlFields="TaskID"
                    DataNavigateUrlFormatString="~/DocTask/ChangeDocTask.aspx?TaskID={0}"
                    HeaderText="选择更改" NavigateUrl="~/DocTask/ChangeDocTask.aspx"
                    Text="任务信息更改" Visible="False" />
                <asp:HyperLinkField DataNavigateUrlFields="TaskID"
                    DataNavigateUrlFormatString="~/DocTask/CheckDocTask.aspx?TaskID={0}" HeaderText="选择确认"
                    NavigateUrl="~/DocTask/CheckDocTask.aspx" Text="任务完成确认" Visible="False" />
                <asp:CommandField HeaderText="选择" ShowDeleteButton="True" Visible="False" />
            </Columns>
            <PagerStyle HorizontalAlign="Center" />
        </asp:GridView>
        <br />
        <br />
    </div>
</asp:Content>
