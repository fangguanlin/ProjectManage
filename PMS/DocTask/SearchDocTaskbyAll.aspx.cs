﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using Model;
using PMS.App_Code;

namespace PMS
{
    public partial class SearchDocTaskbyAll : System.Web.UI.Page
    {
        private string projectID, publisher, executor,department, projectName, productTypeID, customerID, taskStatus, taskName, productTypeStep;
        private int count;
        private DateTime dt1, dt2;
        private DocTask doc;
        private Department dep;

        public SearchDocTaskbyAll()
        {
            doc = new DocTask();
            dep = new Department();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindTaskStatus();
                BindDepartment();
            }
        }

        protected void BindTaskStatus()
        {
            this.DropDownList1.DataSource = TaskStatus_Step.list;
            this.DropDownList1.DataTextField = "StepText";
            this.DropDownList1.DataValueField = "StepValue";
            this.DropDownList1.DataBind();
        }

        protected void BindDepartment()
        {
            this.DropDownList2.DataSource = dep.LoadEntities();
            this.DropDownList2.DataTextField = "DepartmentName";
            this.DropDownList2.DataValueField = "DepartmentName";
            this.DropDownList2.DataBind();
            this.DropDownList2.Items.Insert(0, new ListItem("ALL", ""));
        }

        protected void Button1_Click(object sender, EventArgs e)
        {            
            BindDocTask();
        }

        protected void BindDocTask()
        {
            BindParm();
            DisplayData();
        }

        protected void BindParm()
        {
            projectID = this.TextBox1.Text.Trim();
            projectName = this.TextBox2.Text.Trim();
            productTypeID = this.TextBox3.Text.Trim();
            publisher = this.TextBox4.Text.Trim();
            executor = this.TextBox5.Text.Trim();
            department = this.DropDownList2.SelectedValue;
            customerID = this.TextBox6.Text.Trim();
            taskStatus = this.DropDownList1.SelectedValue;
            taskName = this.TextBox7.Text.Trim();
            productTypeStep = this.TextBox8.Text.Trim();
            if (string.IsNullOrEmpty(this.TextBox9.Text.Trim()))
            {
                dt1 = DateTime.Parse("1/1/2012 00:00:00");
            }
            else
            {
                dt1 = DateTime.Parse(this.TextBox9.Text.Trim());
            }
            if (string.IsNullOrEmpty(this.TextBox10.Text.Trim()))
            {
                dt2 = DateTime.Parse("1/1/2015 00:00:00");
            }
            else
            {
                dt2 = DateTime.Parse(this.TextBox10.Text.Trim());
            }
        }

        protected void DisplayData()
        {
            IList<DocTaskInfo> list = doc.SearchDocTaskbyAll(projectID, publisher, executor, department, projectName, productTypeID, customerID, taskStatus, taskName, productTypeStep, dt1, dt2);
            count = list.Count;
            this.GridView1.DataSource = list;
            this.GridView1.DataBind();
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (DateTime.Parse(GridView1.DataKeys[i].Values[1].ToString()) == DateTime.Parse("1900-01-01 00:00:00"))
                {
                    GridView1.Rows[i].Cells[9].Text = "";
                    GridView1.Rows[i].Cells[15].Text = "";
                }
                if (GridView1.DataKeys[i].Values[2].ToString() != "OPEN" || GridView1.DataKeys[i].Values[3].ToString() != User.Identity.Name)
                {
                    GridView1.Rows[i].Cells[16].Enabled = false;
                    GridView1.Rows[i].Cells[17].Enabled = false;
                }
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //如果搜索条件更改，又不想影响分页则需要用Session记录条件变量
            this.GridView1.PageIndex = e.NewPageIndex;
            BindDocTask();
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    ((LinkButton)e.Row.Cells[18].Controls[0]).Attributes.Add("onclick", "javascript:return confirm('你确认要删除!')");
                }
                if (bool.Parse(GridView1.DataKeys[e.Row.RowIndex].Values[4].ToString()))
                {
                    e.Row.Cells[2].ForeColor = CommData.IMPORTANT_COLOR;
                }
                if (GridView1.DataKeys[e.Row.RowIndex].Values[2].ToString() == "OPEN")
                {
                    e.Row.Cells[10].ForeColor = CommData.OPEN_COLOR;
                }
                if (GridView1.DataKeys[e.Row.RowIndex].Values[2].ToString() == "DELAYCLOSED")
                {
                    e.Row.Cells[10].ForeColor = CommData.DELAYCLOSED_COLOR;
                }
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lbl = (Label)e.Row.FindControl("Label1");
                lbl.Text = count.ToString();
            }
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (User.Identity.Name == GridView1.DataKeys[e.RowIndex].Values[3].ToString())
            {
                int taskID = int.Parse(GridView1.DataKeys[e.RowIndex].Values[0].ToString());
                doc.Delete(taskID, Page.User.Identity.Name);
                BindDocTask();
            }
            else
            {
                string str = "无权删除，只有相应的项目发布人能做此操作！";
                ClientScript.RegisterStartupScript(ClientScript.GetType(), "", "<script>alert('" + str + "');</script>");
            }
        }
    }
}