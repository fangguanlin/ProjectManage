﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewDocTaskDetails.aspx.cs" Inherits="PMS.ViewDocTaskDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
<div style="text-align: center; vertical-align: middle;">    
        <br />
        <br />
        <br />
        <asp:Panel ID="Panel1" runat="server" Width="700px" style="text-align: center; margin:0 auto;">
        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
        </asp:Panel>
        <br />
        <asp:Panel ID="Panel4" runat="server" Visible="false" Width="700px" style="text-align: center; margin:0 auto;">            
            <table width="100%">
                <tr>
                    <td><asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">任务信息更改</asp:LinkButton></td>
                    <td><asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click">任务完成确认</asp:LinkButton></td>
                </tr>
            </table>            
        </asp:Panel>
        <br />
        <asp:Panel ID="Panel2" runat="server" Width="700px" style="text-align: center; margin:0 auto;">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="gridview_m" EmptyDataText="还没有项目信息更改记录哟！">
                <Columns>
                    <asp:BoundField DataField="RecordTime" HeaderText="修改时间" 
                        DataFormatString="{0:d}" />
                    <asp:BoundField DataField="UserID" HeaderText="修改人" />
                    <asp:BoundField DataField="ChangeDetail" HeaderText="修改原因与内容" />
                </Columns>
            </asp:GridView>
                   </asp:Panel>
                   <br />
        <asp:Panel ID="Panel3" runat="server" Width="700px" style="text-align: center; margin:0 auto;">
            <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" Width="100%" EmptyDataText="还没有做完成确认哟！" CellPadding="5" CellSpacing="1">
                <Fields>
                    <asp:BoundField DataField="RecordTime" HeaderText="完成确认时间" 
                        DataFormatString="{0:d}" />
                    <asp:BoundField DataField="UserID" HeaderText="完成确认人" />
                    <asp:BoundField DataField="EndTime" HeaderText="实际完成时间" Visible="False" 
                        DataFormatString="{0:d}" />
                    <asp:BoundField DataField="CloseStatus" HeaderText="完成情况" />
                </Fields>
            </asp:DetailsView>
        </asp:Panel>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
</div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
