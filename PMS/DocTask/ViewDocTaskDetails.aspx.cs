﻿using System;
using System.Collections.Generic;
using BLL;
using Model;

namespace PMS
{
    public partial class ViewDocTaskDetails : System.Web.UI.Page
    {
        private DocTask doc;
        private DocTaskChange dtc;
        private DocTaskCheck dtck;
        private int taskID
        {
            get { return int.Parse(ViewState["taskID"].ToString()); }
            set { ViewState["taskID"] = value; }
        }

        public ViewDocTaskDetails()
        {
            doc = new DocTask();
            dtc = new DocTaskChange();
            dtck = new DocTaskCheck();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["TaskID"] != null)
                {
                    taskID =int.Parse(Request.QueryString["TaskID"]);
                    //Session["TaskID"] = taskID;
                    BindUserControl();
                }
                BindDocTaskChange();
                BindDocTaskCheck();
                BindPanel4();
            }
            //taskID = int.Parse(Session["TaskID"].ToString());
        }

        protected void BindUserControl()
        {
            DocTaskDetail uc = (DocTaskDetail)LoadControl("~/UserControl/DocTaskDetail.ascx");
            uc.taskID = taskID;
            this.PlaceHolder1.Controls.Add(uc);
        }

        protected void BindDocTaskChange()
        {
            this.GridView1.DataSource = dtc.GetDocTaskChangesbyID(taskID);
            this.GridView1.DataBind();
        }

        protected void BindDocTaskCheck()
        {
            DocTaskCheckInfo dtci = dtck.LoadEntity(taskID);
            IList<DocTaskCheckInfo> list = new List<DocTaskCheckInfo>();
            list.Add(dtci);
            this.DetailsView1.DataSource = list;
            this.DetailsView1.DataBind();
        }

        protected void BindPanel4()
        {
            DocTaskInfo doci = doc.LoadEntity(taskID);
            if (doci.TaskStatus == "OPEN" && doci.Publisher == User.Identity.Name)
            {
                this.Panel4.Visible = true;
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/DocTask/ChangeDocTask.aspx?TaskID=" + taskID);
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/DocTask/CheckDocTask.aspx?TaskID=" + taskID);
        }
    }
}