﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using Model;
//using Microsoft.Reporting.WebForms;

namespace PMS
{
    public partial class StatisticDocTaskbyExecutor : System.Web.UI.Page
    {
        private DocTask doc;
        private string yeartime
        {
            get { return ViewState["yeartime"].ToString(); }
            set { ViewState["yeartime"] = value; }
        }

        public StatisticDocTaskbyExecutor()
        {
            doc = new DocTask();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                yeartime = this.DropDownList1.SelectedItem.Text;
                BindExecutor();
                BindCustomer();
                //IList<DocTask_QInfo> list = doc.CountDocTasks_Q();
                ////绑定图表
                //Chart1.DataBindCrossTable(list, "Executor", "QuarterTime", "Total", "Label= Element1,ToolTip=Element{及时完成数#}");
                ////Chart1.DataBindCrossTable(list, "Executor", "QuarterTime", "Total", "Label= Quantity{P},ToolTip=Element{及时完成数#}");
                ////绑定报表
                //ReportViewer1.LocalReport.ReportPath = MapPath("StatisticDocTaskbyExecutor.rdlc");
                ////绑定数据源
                ////注意DataSet1必须和你报表所引用的table 一致
                //ReportDataSource rds = new ReportDataSource("DataSet1", list);//注意这里的name和报表中的一致
                //ReportViewer1.LocalReport.DataSources.Clear();
                //ReportViewer1.LocalReport.DataSources.Add(rds);
                //ReportViewer1.LocalReport.Refresh();
            }
            //if (Session["YearTime"] != null)
            //    yeartime = Session["YearTime"].ToString();
        }

        protected void BindExecutor()
        {
            this.GridView1.DataSource = doc.CountDocTask_AllQ(yeartime);
            this.GridView1.DataBind();
        }

        protected void BindCustomer()
        {
            this.GridView2.DataSource = doc.CountDocTasks_Customer_AllQ(yeartime);
            this.GridView2.DataBind();
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.GridView1.PageIndex = e.NewPageIndex;
            //yeartime = this.DropDownList1.SelectedItem.Text;
            BindExecutor();
        }

        protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.GridView2.PageIndex = e.NewPageIndex;
            //yeartime = this.DropDownList1.SelectedItem.Text;
            BindCustomer();
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            yeartime = this.DropDownList1.SelectedItem.Text;
            //Session["YearTime"] = yeartime;
            BindExecutor();
            BindCustomer();
        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //string yeartime = this.DropDownList1.SelectedItem.Text; 
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    TableCellCollection tcHeader = e.Row.Cells;
                    tcHeader.Clear(); 
                    tcHeader.Add(new TableHeaderCell());
                    tcHeader[0].Text = "实施人"; 
                    tcHeader.Add(new TableHeaderCell());
                    tcHeader[1].Text = "<a href=StatisticDocTaskbyQuarter.aspx?QuarterTime=" + yeartime + "1季度>第1季度</a>";
                    tcHeader.Add(new TableHeaderCell());
                    tcHeader[2].Text = "<a href=StatisticDocTaskbyQuarter.aspx?QuarterTime=" + yeartime + "2季度>第2季度</a>";
                    tcHeader.Add(new TableHeaderCell());
                    tcHeader[3].Text = "<a href=StatisticDocTaskbyQuarter.aspx?QuarterTime=" + yeartime + "3季度>第3季度</a>";
                    tcHeader.Add(new TableHeaderCell());
                    tcHeader[4].Text = "<a href=StatisticDocTaskbyQuarter.aspx?QuarterTime=" + yeartime + "4季度>第4季度</a>";
                    break;
            }
        }

        protected void GridView2_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //string yeartime = this.DropDownList1.SelectedItem.Text;
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    TableCellCollection tcHeader = e.Row.Cells;
                    tcHeader.Clear();
                    tcHeader.Add(new TableHeaderCell());
                    tcHeader[0].Text = "实施人";
                    tcHeader.Add(new TableHeaderCell());
                    tcHeader[1].Text = "<a href=StatisticDocTaskbyQuarter.aspx?QuarterTime=" + yeartime + "1季度>第1季度</a>";
                    tcHeader.Add(new TableHeaderCell());
                    tcHeader[2].Text = "<a href=StatisticDocTaskbyQuarter.aspx?QuarterTime=" + yeartime + "2季度>第2季度</a>";
                    tcHeader.Add(new TableHeaderCell());
                    tcHeader[3].Text = "<a href=StatisticDocTaskbyQuarter.aspx?QuarterTime=" + yeartime + "3季度>第3季度</a>";
                    tcHeader.Add(new TableHeaderCell());
                    tcHeader[4].Text = "<a href=StatisticDocTaskbyQuarter.aspx?QuarterTime=" + yeartime + "4季度>第4季度</a>";
                    break;
            }
        }
    }
}