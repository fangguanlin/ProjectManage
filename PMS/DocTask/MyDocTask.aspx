﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyDocTask.aspx.cs" Inherits="PMS.MyDocTask" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <p>待完成的任务：</p>

    <asp:GridView ID="GridView1" runat="server" AllowPaging="True"
        AutoGenerateColumns="False" PageSize="20" Width="100%" CssClass="gridview_m"
        OnPageIndexChanging="GridView1_PageIndexChanging"
        DataKeyNames="TaskID,EndTime,TaskStatus,Publisher,IsImportant"
        OnRowDataBound="GridView1_RowDataBound"
        OnRowDeleting="GridView1_RowDeleting">
        <Columns>
            <asp:BoundField DataField="ProjectID" HeaderText="项目编号" />
            <asp:BoundField DataField="ProjectName" HeaderText="项目名称" />
            <asp:BoundField DataField="ProductTypeStep" HeaderText="产品阶段" />
            <asp:HyperLinkField DataNavigateUrlFields="TaskID"
                DataNavigateUrlFormatString="~/DocTask/ViewDocTaskDetails.aspx?TaskID={0}"
                DataTextField="TaskName" HeaderText="任务名称"
                NavigateUrl="~/DocTask/ViewDocTaskDetails.aspx" />
            <asp:BoundField DataField="TaskDetail" HeaderText="任务详细" />
            <asp:BoundField DataField="Executor" HeaderText="实施人" />
            <asp:BoundField DataField="Department" HeaderText="实施部门" />
            <asp:BoundField DataField="PostTime" HeaderText="要求完成时间"
                DataFormatString="{0:d}" />
            <asp:BoundField DataField="EndTime" HeaderText="实际完成时间"
                DataFormatString="{0:d}" />
            <asp:BoundField DataField="TaskStatus" HeaderText="任务状态" />
            <asp:BoundField DataField="CloseStatus" HeaderText="完成情况&延期原因" />
            <asp:BoundField DataField="Publisher" HeaderText="发布人" />
            <asp:BoundField DataField="RecordTime" HeaderText="录入时间"
                DataFormatString="{0:d}" Visible="False" />
            <asp:BoundField DataField="BeginTime" HeaderText="开始时间"
                DataFormatString="{0:d}" Visible="False" />
            <asp:BoundField DataField="ConsumeDays" HeaderText="消耗工时" Visible="False" />
            <asp:HyperLinkField DataNavigateUrlFields="TaskID"
                DataNavigateUrlFormatString="~/DocTask/ChangeDocTask.aspx?TaskID={0}"
                HeaderText="选择更改" NavigateUrl="~/DocTask/ChangeDocTask.aspx"
                Text="任务信息更改" Visible="False" />
            <asp:HyperLinkField DataNavigateUrlFields="TaskID"
                DataNavigateUrlFormatString="~/DocTask/CheckDocTask.aspx?TaskID={0}" HeaderText="选择确认"
                NavigateUrl="~/DocTask/CheckDocTask.aspx" Text="任务完成确认" Visible="False" />
            <asp:CommandField HeaderText="选择" ShowDeleteButton="True" Visible="False" />
        </Columns>
    </asp:GridView>

    <br />

    <br />

    <br />
</asp:Content>
