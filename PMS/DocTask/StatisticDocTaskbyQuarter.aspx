﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StatisticDocTaskbyQuarter.aspx.cs" Inherits="PMS.StatisticDocTaskbyQuarter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div>
                <br />
                按实施人统计：<br />
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="80%" CssClass="gridview_m"
                    OnPageIndexChanging="GridView1_PageIndexChanging" AllowPaging="True">
                    <Columns>
                        <asp:BoundField DataField="Executor" HeaderText="实施人" />
                        <asp:BoundField DataField="Total" HeaderText="任务数" />
                        <asp:BoundField DataField="Element2" HeaderText="提前完成数" />
                        <asp:BoundField DataField="Element" HeaderText="及时完成数" />
                        <asp:BoundField DataField="Element3" HeaderText="延期完成数" />
                        <asp:BoundField DataField="Element4" HeaderText="未完成数" />
                        <asp:BoundField DataField="Element1" HeaderText="完成数" />
                        <asp:BoundField DataField="Quantity" DataFormatString="{0:P}" HeaderText="及时率" />
                        <asp:BoundField DataField="Quantity1" DataFormatString="{0:P}" HeaderText="完成率" />
                    </Columns>
                </asp:GridView>
                <asp:Chart ID="Chart1" runat="server" Width="1100px" Height="500px" BorderlineDashStyle="Solid" Palette="BrightPastel" ImageType="Png" 
                    BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BackColor="#D3DFF0" BorderColor="26, 59, 105" Visible="false">
                    <Titles>
                        <asp:Title Font="微软雅黑, 16pt" Name="Title1" Text="按实施人统计任务完成情况表">
                        </asp:Title>
                    </Titles>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>                    
                    <Legends>
                        <asp:legend LegendStyle="Row" IsTextAutoFit="False" DockedToChartArea="ChartArea1" Docking="Bottom" IsDockedInsideChartArea="False" Name="Default" BackColor="Transparent" Font="Trebuchet MS, 8.25pt, style=Bold" Alignment="Center"></asp:legend>
                    </Legends>
                    <Series>   
                        <asp:Series Name="及时率" ChartArea="ChartArea1" BorderWidth="3">
                        </asp:Series>
                        <asp:Series Name="完成率" ChartArea="ChartArea1" BorderWidth="3">                            
                        </asp:Series> 
                        <asp:Series Name="及时完成数" ChartArea="ChartArea2">                            
                        </asp:Series>  
                        <asp:Series Name="延期完成数" ChartArea="ChartArea2">                            
                        </asp:Series>     
                        <asp:Series Name="未完成数" ChartArea="ChartArea2">  
                        </asp:Series>               
                    </Series>
                    <ChartAreas>  
                        <asp:ChartArea Name="ChartArea1">                                                                                    
                        </asp:ChartArea>   
                        <asp:ChartArea Name="ChartArea2">                            
                        </asp:ChartArea>                   
                    </ChartAreas>
                </asp:Chart>
                <br />
                <br />
                按客户统计：<asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
                    OnPageIndexChanging="GridView2_PageIndexChanging" Width="80%" AllowPaging="True">
                    <Columns>
                        <asp:BoundField DataField="Executor" HeaderText="客户名称" />
                        <asp:BoundField DataField="Total" HeaderText="任务数" />
                        <asp:BoundField DataField="Element2" HeaderText="提前完成数" />
                        <asp:BoundField DataField="Element" HeaderText="及时完成数" />
                        <asp:BoundField DataField="Element3" HeaderText="延期完成数" />
                        <asp:BoundField DataField="Element4" HeaderText="未完成数" />
                        <asp:BoundField DataField="Element1" HeaderText="完成数" />
                        <asp:BoundField DataField="Quantity" DataFormatString="{0:P}" 
                            HeaderText="及时率" />
                        <asp:BoundField DataField="Quantity1" DataFormatString="{0:P}" 
                            HeaderText="完成率" />
                    </Columns>
                </asp:GridView>
                <asp:Chart ID="Chart2" runat="server" Width="1100px" Height="500px" BorderlineDashStyle="Solid" Palette="BrightPastel" ImageType="Png" 
                    BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BackColor="#D3DFF0" BorderColor="26, 59, 105" Visible="false">
                    <Titles>
                        <asp:Title Font="微软雅黑, 16pt" Name="Title1" Text="按客户统计任务完成情况表">
                        </asp:Title>
                    </Titles>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <Legends>
                        <asp:legend LegendStyle="Row" IsTextAutoFit="False" DockedToChartArea="ChartArea1" Docking="Bottom" IsDockedInsideChartArea="False" Name="Default" BackColor="Transparent" Font="Trebuchet MS, 8.25pt, style=Bold" Alignment="Center"></asp:legend>
                    </Legends>
                    <Series>
                        <asp:Series Name="及时率" ChartArea="ChartArea1">
                        </asp:Series>
                        <asp:Series Name="完成率" ChartArea="ChartArea1">                            
                        </asp:Series>     
                        <asp:Series Name="及时完成数" ChartArea="ChartArea2">                            
                        </asp:Series>  
                        <asp:Series Name="延期完成数" ChartArea="ChartArea2">                            
                        </asp:Series>     
                        <asp:Series Name="未完成数" ChartArea="ChartArea2">                            
                        </asp:Series>                  
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1">                                                                                    
                        </asp:ChartArea>   
                        <asp:ChartArea Name="ChartArea2">                            
                        </asp:ChartArea>                      
                    </ChartAreas>
                </asp:Chart>
                <br />
                <br />
                <br />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
