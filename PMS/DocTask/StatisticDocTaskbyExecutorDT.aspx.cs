﻿using System;
using System.Collections.Generic;
using BLL;
using System.Web.UI.DataVisualization.Charting;
//using Microsoft.Reporting.WebForms;
using Model;

namespace PMS
{
    public partial class StatisticDocTaskbyExecutorDT : System.Web.UI.Page
    {
        private DocTask doc;
        private DateTime starttime
        {
            get { return DateTime.Parse(ViewState["starttime"].ToString()); }
            set { ViewState["starttime"] = value; }
        }
        private DateTime stoptime
        {
            get { return DateTime.Parse(ViewState["stoptime"].ToString()); }
            set { ViewState["stoptime"] = value; }
        }
        public StatisticDocTaskbyExecutorDT()
        {
            doc = new DocTask();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            starttime = DateTime.Parse(this.TextBox1.Text.Trim());
            stoptime = DateTime.Parse(this.TextBox2.Text.Trim());
            BindGV1();
            BindGV2();
            BindReport1();
            BindReport2();
        }

        protected void BindGV1()
        {
            this.GridView1.DataSource = doc.CountDocTasks_DT(starttime, stoptime);
            this.GridView1.DataBind();
        }

        protected void BindGV2()
        {
            this.GridView2.DataSource = doc.CountDocTasks_Customer_DT(starttime, stoptime);
            this.GridView2.DataBind();
        }

        protected void BindReport1()
        {
            IList<DocTask_QInfo> list = doc.CountDocTasks_DT(starttime, stoptime);
            Chart1.Series["及时率"].Points.DataBind(list, "Executor", "Quantity", "");
            Chart1.Series["完成率"].Points.DataBind(list, "Executor", "Quantity1", "");
            Chart1.Series["及时完成数"].Points.DataBind(list, "Executor", "Element", "Label= Element{#}");
            Chart1.Series["延期完成数"].Points.DataBind(list, "Executor", "Element3", "Label= Element3{#}");
            Chart1.Series["未完成数"].Points.DataBind(list, "Executor", "Element4", "Label= Element4{#}");
            Chart1.Series["及时率"].ChartType = SeriesChartType.Line;
            Chart1.Series["完成率"].ChartType = SeriesChartType.Line;
            Chart1.Series["及时完成数"].ChartType = SeriesChartType.StackedColumn;
            Chart1.Series["延期完成数"].ChartType = SeriesChartType.StackedColumn;
            Chart1.Series["未完成数"].ChartType = SeriesChartType.StackedColumn;
            Chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = 90;
            Chart1.ChartAreas["ChartArea1"].AxisX.Interval = 1;
            Chart1.ChartAreas["ChartArea2"].AxisX.LabelStyle.Angle = 90;
            Chart1.ChartAreas["ChartArea2"].AxisX.Interval = 1;
            this.Chart1.Visible = true;
        }

        protected void BindReport2()
        {
            IList<DocTask_QInfo> list = doc.CountDocTasks_Customer_DT(starttime, stoptime);
            Chart2.Series["及时率"].Points.DataBind(list, "Executor", "Quantity", "Label= Quantity{P}");            
            Chart2.Series["完成率"].Points.DataBind(list, "Executor", "Quantity1", "Label= Quantity1{P}");
            Chart2.Series["及时完成数"].Points.DataBind(list, "Executor", "Element", "Label= Element{#}");
            Chart2.Series["延期完成数"].Points.DataBind(list, "Executor", "Element3", "Label= Element1{#}");
            Chart2.Series["未完成数"].Points.DataBind(list, "Executor", "Element4", "Label= Total{#}");
            this.Chart2.Visible = true;
            //Chart1.DataBindTable(list, "Executor");
            //Chart1.DataBindCrossTable(list, "Executor", "QuarterTime", "Total", "Label= Quantity{P},ToolTip=Element{完成数#}");
        }

        //protected void BindReport()
        //{
        //    //ReportViewer1.Visible = true;
        //    IList<DocTask_QInfo> list = doc.CountDocTasks_DT(starttime, stoptime);
        //    //Chart1.DataBindTable(list, "Executor");
        //    //Chart1.DataBindCrossTable(list, "Executor", "QuarterTime", "Total", "Label= Quantity{P},ToolTip=Element{完成数#}");
        //    //绑定报表
        //    ReportViewer1.LocalReport.ReportPath = MapPath("StatisticDocTaskbyExecutorDT.rdlc");
        //    //绑定数据源
        //    //注意DataSet1必须和你报表所引用的table 一致
        //    ReportDataSource rds = new ReportDataSource("DataSet2", list);//注意这里的name和报表中的一致
        //    ReportViewer1.LocalReport.DataSources.Clear();
        //    ReportViewer1.LocalReport.DataSources.Add(rds);
        //    ReportViewer1.LocalReport.Refresh();
        //}

        protected void GridView1_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            this.GridView1.PageIndex = e.NewPageIndex;
            Button1_Click(sender, e);
        }

        protected void GridView2_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            this.GridView2.PageIndex = e.NewPageIndex;
            Button1_Click(sender, e);
        }
    }
}