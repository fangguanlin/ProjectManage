﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ViewDocTaskbyProject.aspx.cs" Inherits="PMS.ViewDocTaskbyProject" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="../Scripts/GetSingleCheckBox.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table width="100%">
                    <tr>
                        <td style=" width:80%; text-align:left;">项目<asp:Label ID="Label1" runat="server"></asp:Label>中的任务：</td>
                        <td style=" width:10%; text-align:right;"><asp:LinkButton runat="server" ID="LinkButton1" OnClick="LinkButton1_Click">
                            新增任务</asp:LinkButton></td>
                        <td style=" width:10%; text-align:right;">
                            <asp:LinkButton runat="server" ID="LinkButton2" OnClick="LinkButton2_Click" OnClientClick="return confirm( '确认删除此任务？');">
                                删除任务</asp:LinkButton></td>
                    </tr>
                </table>   
                <%--正常模板左对齐与空模板居中对齐--%>             
                <div <%--style="text-align: center; margin: 0 auto;"--%>>   
                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="20" Width="100%" OnPageIndexChanging="GridView1_PageIndexChanging"
                        DataKeyNames="TaskID,EndTime,TaskStatus,Publisher,IsImportant" OnRowDataBound="GridView1_RowDataBound" CellPadding="2"
                        OnRowDeleting="GridView1_RowDeleting" OnRowCommand="GridView1_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="序号">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex +1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ProjectID" HeaderText="项目编号" Visible="False" />
                            <asp:BoundField DataField="ProjectName" HeaderText="项目名称" Visible="False" />
                            <asp:BoundField DataField="ProductTypeStep" HeaderText="产品阶段" />
                            <asp:HyperLinkField DataNavigateUrlFields="TaskID" DataNavigateUrlFormatString="~/DocTask/ViewDocTaskDetails.aspx?TaskID={0}"
                                DataTextField="TaskName" HeaderText="任务名称" NavigateUrl="~/DocTask/ViewDocTaskDetails.aspx" />
                            <asp:BoundField DataField="TaskDetail" HeaderText="任务详细" />
                            <asp:BoundField DataField="Executor" HeaderText="实施人" />
                            <asp:BoundField DataField="Department" HeaderText="实施部门" />
                            <asp:BoundField DataField="PostTime" HeaderText="要求完成时间" DataFormatString="{0:d}" />
                            <asp:BoundField DataField="EndTime" HeaderText="实际完成时间" DataFormatString="{0:d}" />
                            <asp:BoundField DataField="TaskStatus" HeaderText="任务状态" />
                            <asp:BoundField DataField="CloseStatus" HeaderText="完成情况&延期原因" />
                            <asp:BoundField DataField="Publisher" HeaderText="发布人" />
                            <asp:BoundField DataField="RecordTime" HeaderText="录入时间" DataFormatString="{0:d}"
                                Visible="False" />
                            <asp:BoundField DataField="BeginTime" HeaderText="开始时间" DataFormatString="{0:d}"
                                Visible="False" />
                            <asp:BoundField DataField="ConsumeDays" HeaderText="消耗工时" Visible="False" />
                            <asp:HyperLinkField DataNavigateUrlFields="TaskID" DataNavigateUrlFormatString="~/DocTask/ChangeDocTask.aspx?TaskID={0}"
                                HeaderText="选择更改" NavigateUrl="~/DocTask/ChangeDocTask.aspx" Text="任务信息更改" Visible="False" />
                            <asp:HyperLinkField DataNavigateUrlFields="TaskID" DataNavigateUrlFormatString="~/DocTask/CheckDocTask.aspx?TaskID={0}"
                                HeaderText="选择确认" NavigateUrl="~/DocTask/CheckDocTask.aspx" Text="任务完成确认" Visible="False" />
                            <asp:CommandField HeaderText="选择" ShowDeleteButton="True" Visible="False" />
                            <asp:ButtonField CommandName="Insert" HeaderText="选择" Text="新增" Visible="False" />
                            <asp:TemplateField HeaderText=" ">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server"/> 
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle BorderColor="Black" />
                        <EmptyDataRowStyle HorizontalAlign="Center"/>
                        <EmptyDataTemplate>
                            <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" CssClass=""
                                DefaultMode="Insert" OnItemInserting="DetailsView1_ItemInserting" Height="400px" Width="30%"
                                OnModeChanging="DetailsView1_ModeChanging" AutoGenerateInsertButton="True">
                                <Fields>
                                    <asp:TemplateField HeaderText="项目编号">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("ProjectID") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("ProjectID") %>'></asp:Label>
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("ProjectID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="任务名称">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("TaskName") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:DropDownList ID="DDLTaskName" runat="server">
                                            </asp:DropDownList>
                                            <br />
                                            其它：<asp:TextBox ID="TbTaskName" runat="server" Text='<%# Bind("TaskName") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("TaskName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="产品阶段">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("ProductTypeStep") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:DropDownList ID="DDLProductTypeStep" runat="server">
                                            </asp:DropDownList>
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("ProductTypeStep") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="开始时间">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("BeginTime") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("BeginTime") %>'></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="TextBox1_CalendarExtender" runat="server" Enabled="True" CssClass=""
                                                TargetControlID="TextBox1">
                                            </ajaxToolkit:CalendarExtender>
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("BeginTime") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="要求完成时间">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("PostTime") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("PostTime") %>'></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="TextBox2_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="TextBox2">
                                            </ajaxToolkit:CalendarExtender>
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("PostTime") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                    
                                    <asp:TemplateField HeaderText="实施部门">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("Department") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:DropDownList ID="DDLDepartment" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DDLDepartment_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("Department") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="实施人">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("Executor") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:DropDownList ID="DDLExecutor" runat="server">
                                            </asp:DropDownList>
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label8" runat="server" Text='<%# Bind("Executor") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="任务详细">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("TaskDetail") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TBXTaskDetail" runat="server" Text='<%# Bind("TaskDetail") %>' Height="62px" TextMode="MultiLine"></asp:TextBox>
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("TaskDetail") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Fields>
                            </asp:DetailsView>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <br />
                <br />
            </div>
            <div>
                <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBox1_CheckedChanged" />  显示已完结任务
                <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="TaskID,EndTime,TaskStatus,Publisher,IsImportant" 
                    OnPageIndexChanging="GridView2_PageIndexChanging" OnRowDataBound="GridView2_RowDataBound" PageSize="20" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="序号">
                            <ItemTemplate>
                                <%# Container.DataItemIndex +1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ProjectID" HeaderText="项目编号" Visible="False" />
                        <asp:BoundField DataField="ProjectName" HeaderText="项目名称" Visible="False" />
                        <asp:BoundField DataField="ProductTypeStep" HeaderText="产品阶段" />
                        <asp:HyperLinkField DataNavigateUrlFields="TaskID" DataNavigateUrlFormatString="~/DocTask/ViewDocTaskDetails.aspx?TaskID={0}" DataTextField="TaskName" HeaderText="任务名称" NavigateUrl="~/DocTask/ViewDocTaskDetails.aspx" />
                        <asp:BoundField DataField="TaskDetail" HeaderText="任务详细" />
                        <asp:BoundField DataField="Executor" HeaderText="实施人" />
                        <asp:BoundField DataField="Department" HeaderText="实施部门" />
                        <asp:BoundField DataField="PostTime" DataFormatString="{0:d}" HeaderText="要求完成时间" />
                        <asp:BoundField DataField="EndTime" DataFormatString="{0:d}" HeaderText="实际完成时间" />
                        <asp:BoundField DataField="TaskStatus" HeaderText="任务状态" />
                        <asp:BoundField DataField="CloseStatus" HeaderText="完成情况&amp;延期原因" />
                        <asp:BoundField DataField="Publisher" HeaderText="发布人" />
                        <asp:BoundField DataField="RecordTime" DataFormatString="{0:d}" HeaderText="录入时间" Visible="False" />
                        <asp:BoundField DataField="BeginTime" DataFormatString="{0:d}" HeaderText="开始时间" Visible="False" />
                        <asp:BoundField DataField="ConsumeDays" HeaderText="消耗工时" Visible="False" />
                    </Columns>                    
                </asp:GridView>
                <br />
                <br />
                <br />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
