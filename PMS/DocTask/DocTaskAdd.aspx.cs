﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using BLL;
using Model;

namespace PMS
{
    public partial class DocTaskAdd : System.Web.UI.Page
    {
        private DocTask doc;

        public DocTaskAdd()
        {
            doc = new DocTask();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ProjectID"] != null)
                {
                    this.TextBox1.Text = Request.QueryString["ProjectID"];
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            DocTaskInfo docinfo = new DocTaskInfo(1, this.TextBox1.Text.Trim(), this.TextBox2.Text.Trim(),"", DateTime.Now, User.Identity.Name
                , DateTime.Parse(this.TextBox3.Text.Trim()), DateTime.Parse(this.TextBox4.Text.Trim()), this.TextBox5.Text.Trim()
                , this.TextBox6.Text.Trim(), this.TextBox8.Text.Trim(), DateTime.Now, 0, "OPEN");
            string str = "";
            try
            {
                doc.Insert(docinfo, Page.User.Identity.Name);
                str = "任务发布成功！";
                this.TextBox1.Text = "";
                this.TextBox2.Text = "";
                this.TextBox3.Text = "";
                this.TextBox4.Text = "";
                this.TextBox5.Text = "";
                this.TextBox6.Text = "";
                this.TextBox8.Text = "";
            }
            catch
            {
                str = "任务发布失败，请联系相关人员！";                
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            string proID = this.TextBox9.Text.Trim();
            string taskName = this.TextBox10.Text.Trim();
            DocTaskInfo doci = doc.GetDocTaskbyPIDName(proID, taskName);
            IList<DocTaskInfo> list = new List<DocTaskInfo>();
            list.Add(doci);
            this.GridView2.DataSource = list;
            this.GridView2.DataBind();
        }
    }
}