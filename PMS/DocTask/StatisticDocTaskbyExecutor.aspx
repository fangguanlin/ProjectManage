﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StatisticDocTaskbyExecutor.aspx.cs" Inherits="PMS.StatisticDocTaskbyExecutor" %>

<%--<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    选择日历年：<asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
        <asp:ListItem>2014年</asp:ListItem>
        <asp:ListItem>2013年</asp:ListItem>
    </asp:DropDownList>
    <br />
    <br />
    按实施人统计：<br />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" 
        OnPageIndexChanging="GridView1_PageIndexChanging" Width="80%" CssClass="gridview_m" OnRowCreated="GridView1_RowCreated">
        <Columns>
            <asp:BoundField DataField="Executor" HeaderText="实施人" />
            <asp:BoundField DataField="第1季度" HeaderText="第1季度" DataFormatString="{0:P}" />
            <asp:BoundField DataField="第2季度" HeaderText="第2季度" DataFormatString="{0:P}" />
            <asp:BoundField DataField="第3季度" HeaderText="第3季度" DataFormatString="{0:P}" />
            <asp:BoundField DataField="第4季度" HeaderText="第4季度" DataFormatString="{0:P}" />            
        </Columns>
    </asp:GridView>

    <%--<rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Width="800px" 
        Font-Size="8pt" InteractiveDeviceInfos="(集合)" WaitMessageFont-Names="Verdana" 
        WaitMessageFont-Size="14pt">
        <LocalReport ReportPath="DocTask\StatisticDocTaskbyExecutor.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="DataSet1" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>

    <rsweb:ReportViewer ID="ReportViewer2" runat="server"></rsweb:ReportViewer>
    
    <asp:Chart ID="Chart1" runat="server" Width="800px" BorderDashStyle="Solid" Palette="BrightPastel" imagetype="Png"  BackSecondaryColor="White" 
    BackGradientStyle="TopBottom" BorderWidth="2" backcolor="#D3DFF0" BorderColor="26, 59, 105">
        <Titles>
                <asp:Title Font="微软雅黑, 16pt" Name="Title1" Text="实施人任务完成情况季度统计表">
                </asp:Title>
            </Titles>
        <borderskin skinstyle="Emboss"></borderskin>  
        <Legends>
            <asp:Legend Title="实施人" Alignment="Center"></asp:Legend>
        </Legends>
        <Series>
            <%--<asp:Series Name="Series1" ChartType="Bubble" MarkerSize="8" MarkerStyle="Circle">
            </asp:Series>
            <asp:Series Name="Series2" ChartType="Bubble" MarkerSize="8" MarkerStyle="Circle">
            </asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="64, 165, 191, 228" 
            ShadowColor="Transparent" BackGradientStyle="TopBottom">
            <AxisY Title="总任务数"></AxisY>
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>--%>
    <br />
    按客户统计：<br />
    <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AutoGenerateColumns="False" 
        OnPageIndexChanging="GridView1_PageIndexChanging" Width="80%" CssClass="gridview_m" OnRowCreated="GridView2_RowCreated">
        <Columns>
            <asp:BoundField DataField="Executor" HeaderText="客户" />
            <asp:BoundField DataField="第1季度" HeaderText="第1季度" DataFormatString="{0:P}" />
            <asp:BoundField DataField="第2季度" HeaderText="第2季度" DataFormatString="{0:P}" />
            <asp:BoundField DataField="第3季度" HeaderText="第3季度" DataFormatString="{0:P}" />
            <asp:BoundField DataField="第4季度" HeaderText="第4季度" DataFormatString="{0:P}" />
        </Columns>
    </asp:GridView>

    <br />
    <br />
    <br />
    <br />
    <br />

</asp:Content>
