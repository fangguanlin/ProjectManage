﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CheckDocTask.aspx.cs" Inherits="PMS.CheckDocTask" %>

<%@ Register src="../UserControl/DocTaskDetail.ascx" tagname="DocTaskDetail" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<script type="text/javascript" src="../Scripts/my97datepicker/WdatePicker.js"></script>--%>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
<div style="text-align: center; vertical-align: middle;">    
        <br />
        <br />
        <br />
        <asp:Panel ID="Panel1" runat="server" Width="700px" style="text-align: center; margin:0 auto;">
        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder></asp:Panel>
        <br />
        <p>
            实际完成时间：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <ajaxToolkit:CalendarExtender ID="TextBox1_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="TextBox1">
            </ajaxToolkit:CalendarExtender>
&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="TextBox1" ErrorMessage="*" 
                ValidationGroup="VGroup1" ForeColor="Red"></asp:RequiredFieldValidator>
                    </p>
        <p style=" vertical-align:middle; ">
            完成情况&延期原因：<asp:TextBox ID="TextBox2" runat="server" Height="62px" TextMode="MultiLine" 
                Width="234px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                ControlToValidate="TextBox2" ErrorMessage="*" ForeColor="Red" 
                ValidationGroup="VGroup1"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                ControlToValidate="TextBox2" ErrorMessage="*" ForeColor="Red" 
                ValidationGroup="VGroup2"></asp:RequiredFieldValidator>
        </p>
        <p>
        <asp:Button ID="Button2" runat="server" onclick="Button2_Click" Text="完成确认" OnClientClick="return confirm( '确认此任务完成？'); " 
                ValidationGroup="VGroup1" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button3" runat="server" onclick="Button3_Click" Text="未完成确认" OnClientClick="return confirm( '确认此任务未完成？'); " 
                ValidationGroup="VGroup2" />
        </p>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
</div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
