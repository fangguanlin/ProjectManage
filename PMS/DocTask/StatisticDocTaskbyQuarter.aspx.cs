﻿using BLL;
using Model;
using System;
using System.Web.UI.DataVisualization.Charting;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace PMS
{
    public partial class StatisticDocTaskbyQuarter : System.Web.UI.Page
    {
        private DocTask doc;
        private string quartertime
        {
            get { return ViewState["quartertime"].ToString(); }
            set { ViewState["quartertime"] = value; }
        }
        public StatisticDocTaskbyQuarter()
        {
            doc = new DocTask();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                quartertime = Request.QueryString["QuarterTime"].ToString();
                //Session["QuarterTime"] = quartertime;
                BindGV1();
                BindGV2();
                BindReport1();
                BindReport2();
            }
            //quartertime = Session["QuarterTime"].ToString();
            BindReport1();
            BindReport2();
        }

        protected void BindGV1()
        {
            this.GridView1.DataSource = doc.CountDocTasks_Q(quartertime);
            this.GridView1.DataBind();
        }

        protected void BindGV2()
        {
            this.GridView2.DataSource = doc.CountDocTasks_Customer_Q(quartertime);
            this.GridView2.DataBind();
        }

        protected void BindReport1()
        {
            IList<DocTask_QInfo> list = doc.CountDocTasks_Q(quartertime);
            Chart1.Series["及时率"].Points.DataBind(list, "Executor", "Quantity", "");
            Chart1.Series["完成率"].Points.DataBind(list, "Executor", "Quantity1", "");
            Chart1.Series["及时完成数"].Points.DataBind(list, "Executor", "Element", "Label= Element{#}");
            Chart1.Series["延期完成数"].Points.DataBind(list, "Executor", "Element3", "Label= Element3{#}");
            Chart1.Series["未完成数"].Points.DataBind(list, "Executor", "Element4", "Label= Element4{#}");
            Chart1.Series["及时率"].ChartType = SeriesChartType.Line;
            Chart1.Series["完成率"].ChartType = SeriesChartType.Line;
            Chart1.Series["及时完成数"].ChartType = SeriesChartType.StackedColumn;
            Chart1.Series["延期完成数"].ChartType = SeriesChartType.StackedColumn;
            Chart1.Series["未完成数"].ChartType = SeriesChartType.StackedColumn;
            Chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = 90;
            Chart1.ChartAreas["ChartArea1"].AxisX.Interval = 1;
            Chart1.ChartAreas["ChartArea2"].AxisX.LabelStyle.Angle = 90;
            Chart1.ChartAreas["ChartArea2"].AxisX.Interval = 1;
            this.Chart1.Visible = true;
        }

        protected void BindReport2()
        {
            IList<DocTask_QInfo> list = doc.CountDocTasks_Customer_Q(quartertime);
            Chart2.Series["及时率"].Points.DataBind(list, "Executor", "Quantity", "Label= Quantity{P}");
            Chart2.Series["完成率"].Points.DataBind(list, "Executor", "Quantity1", "Label= Quantity1{P}");
            Chart2.Series["及时完成数"].Points.DataBind(list, "Executor", "Element", "Label= Element{#}");
            Chart2.Series["延期完成数"].Points.DataBind(list, "Executor", "Element3", "Label= Element1{#}");
            Chart2.Series["未完成数"].Points.DataBind(list, "Executor", "Element4", "Label= Total{#}");
            this.Chart2.Visible = true;
        }
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.GridView1.PageIndex = e.NewPageIndex;
            BindGV1();
        }

        protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.GridView2.PageIndex = e.NewPageIndex;
            BindGV2();
        }
    }
}