﻿using System;
using System.Web.UI;
using BLL;
using Model;
using PMS.App_Code;
using System.Web.UI.WebControls;

namespace PMS
{
    public partial class ChangeDocTask : System.Web.UI.Page
    {
        private DocTask doc;
        private DocTaskInfo dti;
        private DocTaskChange dtc;
        protected Department dep;
        protected UserInDepartment uid;
        private int taskID
        {
            get { return int.Parse(ViewState["taskID"].ToString()); }
            set { ViewState["taskID"] = value; }
        }

        public ChangeDocTask()
        {
            doc = new DocTask();
            dtc = new DocTaskChange();
            dep = new Department();
            uid = new UserInDepartment();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindTaskName();
                BindProductTypeStep();
                BindDepartment();
                BindExecutor();
                if (Request.QueryString["TaskID"] != null)
                {
                    taskID = int.Parse(Request.QueryString["TaskID"]);
                    //Session["TaskID"] = taskID;
                    dti = doc.LoadEntity(taskID);
                    BindDocTask();
                }
            }
            //taskID = int.Parse(Session["TaskID"].ToString());
            dti = doc.LoadEntity(taskID);
        }

        protected void BindTaskName()
        {
            this.DDLTaskName.DataSource = TaskName_Step.list;
            this.DDLTaskName.DataTextField = "StepText";
            this.DDLTaskName.DataValueField = "StepValue";
            this.DDLTaskName.DataBind();
        }

        protected void BindProductTypeStep()
        {
            this.DropDownList1.DataSource = ProjectProductType_Step.list;
            this.DropDownList1.DataTextField = "StepText";
            this.DropDownList1.DataValueField = "StepValue";
            this.DropDownList1.DataBind();
        }

        protected void BindDepartment()
        {
            this.DropDownList2.DataSource = dep.LoadEntities();
            this.DropDownList2.DataTextField = "DepartmentName";
            this.DropDownList2.DataValueField = "DepartmentID";
            this.DropDownList2.DataBind();
            this.DropDownList2.Items.Add(new ListItem("", "0"));
        }

        protected void BindExecutor()
        {
            int departmentID = int.Parse(this.DropDownList2.SelectedValue);
            this.DropDownList3.DataSource = uid.UserInDepartment_GetbyDID(departmentID);
            this.DropDownList3.DataTextField = "UserName";
            this.DropDownList3.DataValueField = "UserName";
            this.DropDownList3.DataBind();
            if (this.DropDownList3.Items.Count == 0)
            {
                this.DropDownList3.Items.Add(new ListItem("", ""));
            }
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindExecutor();
        }

        protected void BindDocTask()
        {
            this.Label1.Text = dti.ProjectID;
            this.DDLTaskName.SelectedValue = dti.TaskName;
            this.TextBox1.Text = dti.TaskName;
            this.DropDownList1.SelectedValue = dti.ProductTypeStep;
            this.TextBox3.Text = dti.BeginTime.ToString();
            this.TextBox4.Text = dti.PostTime.ToString();
            BindDepartment();
            ListItem option = this.DropDownList2.Items.FindByText(dti.Department);
            if (option != null)
            {
                option.Selected = true;
            }
            //this.DropDownList2.SelectedValue = dep.Department_GetbyName(dti.Department).DepartmentID.ToString();
            BindExecutor();
            //this.DropDownList3.SelectedValue = dti.Executor;
            ListItem option1 = this.DropDownList3.Items.FindByText(dti.Executor);
            if (option1 != null)
            {
                option1.Selected = true;
            }
            this.TextBox8.Text = dti.TaskDetail;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string taskName;
            if (!string.IsNullOrEmpty(this.DDLTaskName.SelectedValue))
            {
                taskName = this.DDLTaskName.SelectedValue;
            }
            else
            {
                taskName = this.TextBox1.Text.Trim();
            }
            DocTaskInfo newdti = new DocTaskInfo(dti.TaskID, dti.ProjectID, taskName, this.DropDownList1.SelectedValue, dti.RecordTime, dti.Publisher, DateTime.Parse(this.TextBox3.Text.Trim())
                , DateTime.Parse(this.TextBox4.Text.Trim()), this.DropDownList3.SelectedItem.Text, this.DropDownList2.SelectedItem.Text, this.TextBox8.Text.Trim(), DateTime.Now, 0, "OPEN");
            DocTaskChangeInfo dtci = new DocTaskChangeInfo(dti.TaskID, DateTime.Now, User.Identity.Name, this.TextBox9.Text.Trim());
            try
            {
                doc.Update(newdti, Page.User.Identity.Name);
                dtc.Insert(dtci, Page.User.Identity.Name);
                string str = "任务信息更改成功！";
                ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
                ClearControl();
            }
            catch
            {
                string str = "任务信息更改失败，请联系相关人员！";
                ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
            }
        }

        protected void ClearControl()
        {
            this.Label1.Text = "";
            this.DDLTaskName.SelectedValue = "";
            this.TextBox1.Text = "";
            this.TextBox3.Text = "";
            this.TextBox4.Text = "";
            //this.DropDownList2.SelectedValue = "";
            //this.DropDownList3.SelectedValue = "";
            this.TextBox8.Text = "";
        }        
    }
}