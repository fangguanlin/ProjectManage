﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocTaskDetail.ascx.cs" Inherits="PMS.DocTaskDetail" %>
<div style="text-align:center; border:1px solid Black;">
    <asp:DetailsView ID="DetailsView1" runat="server" Width="696px"  
        AutoGenerateRows="False" BackColor="White" BorderColor="White" 
        BorderStyle="Ridge" BorderWidth="2px" CellPadding="5" CellSpacing="1" 
        DataKeyNames="TaskID,EndTime,TaskStatus,Publisher,IsImportant" 
        GridLines="None" ondatabound="DetailsView1_DataBound">
        <EditRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
        <Fields>
            <asp:BoundField DataField="ProjectID" HeaderText="项目编号" />
            <asp:BoundField DataField="ProjectName" HeaderText="项目名称" />
            <asp:BoundField DataField="ProductTypeStep" HeaderText="产品阶段" />
            <asp:BoundField DataField="TaskName" HeaderText="任务名称" />
            <asp:BoundField DataField="TaskDetail" HeaderText="任务详细" />
            <asp:BoundField DataField="Executor" HeaderText="实施人" />
            <asp:BoundField DataField="Department" HeaderText="实施部门" />
            <asp:BoundField DataField="PostTime" HeaderText="要求完成时间" 
                DataFormatString="{0:d}" />
            <asp:BoundField DataField="EndTime" HeaderText="实际完成时间" 
                DataFormatString="{0:d}" />
            <asp:BoundField DataField="TaskStatus" HeaderText="任务状态" />
            <asp:BoundField DataField="CloseStatus" HeaderText="完成情况&amp;延期原因" />
            <asp:BoundField DataField="Publisher" HeaderText="发布人" />
            <asp:BoundField DataField="RecordTime" HeaderText="录入时间" 
                DataFormatString="{0:d}" />
            <asp:BoundField DataField="BeginTime" HeaderText="开始时间" 
                DataFormatString="{0:d}" />
            <asp:BoundField DataField="ConsumeDays" HeaderText="消耗工时" />
            <asp:TemplateField HeaderText="是否重点">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("IsImportant") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("IsImportant") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="CheckBox1" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsImportant")) %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Fields>
        <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
        <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
        <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
    </asp:DetailsView>
</div>