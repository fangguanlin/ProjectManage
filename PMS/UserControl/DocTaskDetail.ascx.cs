﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using BLL;
using Model;

namespace PMS
{
    public partial class DocTaskDetail : System.Web.UI.UserControl
    {
        /*
         Set the PlaceHolder's width as 700px for this UserControl
         */

        private DocTask doc;
        private int _taskID;

        public int taskID
        {
            get { return _taskID; }
            set { _taskID = value; }
        }

        public DocTaskDetail()
        {
            doc = new DocTask();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindDocTask();
        }

        protected void BindDocTask()
        {
            IList<DocTaskInfo> list = new List<DocTaskInfo>();
            DocTaskInfo dti = doc.LoadEntity(taskID);
            list.Add(dti);
            this.DetailsView1.DataSource = list;
            this.DetailsView1.DataBind();
        }

        protected void DetailsView1_DataBound(object sender, EventArgs e)
        {
            DetailsView d = (DetailsView)sender;
            if (DateTime.Parse(d.DataKey.Values[1].ToString()) == DateTime.Parse("1900-01-01 00:00:00"))
            {                
                d.Rows[8].Cells[1].Text = "";
                d.Rows[14].Cells[1].Text = "";
            }
            if(bool.Parse(d.DataKey.Values[4].ToString()))
            {
                d.Rows[1].ForeColor = System.Drawing.Color.Red;
            }
            if (d.DataKey.Values[2].ToString() == "OPEN")
            {
                d.Rows[9].ForeColor = System.Drawing.Color.Red;
            }
        }
    }
}