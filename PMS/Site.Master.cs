﻿using BLL;
using Model;
using System;
using System.Web;

namespace PMS
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        private UserInDepartment uid;

        public SiteMaster()
        {
            uid = new UserInDepartment();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                InvalidUser();                
            }
        }

        protected void InvalidUser()
        {
            if (!IsValidUser())
            {
                this.NavigationMenu.Items[4].Selectable = false;
            }
        }

        protected bool IsValidUser()
        {
            foreach (UserInDepartmentInfo uidi in uid.UserInDepartment_GetbyUID(HttpContext.Current.User.Identity.Name))
            {
                //射频综合管理部
                if (uidi.DepartmentID == 9)
                    return true;
            }
            return false;
        }
    }
}
