﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS.App_Code
{
    public static class CommData
    {
        public static string gdPageIndex = "0";
        public static System.Drawing.Color IMPORTANT_COLOR = System.Drawing.Color.Red;
        public static System.Drawing.Color OPEN_COLOR = System.Drawing.Color.Red;
        public static System.Drawing.Color DELAYCLOSED_COLOR = System.Drawing.Color.DarkOrange;
        //用到的Session：gvPageIndex，PageIndex，(ProjectID,TaskID,UserID，ProductTypeTable,TaskID,YearTime,QuarterTime)，括号中的都用ViewState替代了
    }

    public class Step
    {
        public Step(string steptext, string stepvalue)
        {
            this.StepText = steptext;
            this.StepValue = stepvalue;
        }

        public string StepText { get; set; }
        public string StepValue { get; set; }
    }

    public static class Customer_Step
    {
        public static IList<Step> list = new List<Step>()
        {
            new Step("中兴","中兴"),
            new Step("阿朗","阿朗"),
            new Step("诺西","诺西"),
            new Step("泰科","泰科")
        };
    }

    public static class ProjectProductType_Step
    {
        public static IList<Step> list = new List<Step>()
        {
            new Step("客户应答","客户应答"),
            new Step("初版样机","初版样机"),
            new Step("正式样机","正式样机"),
            new Step("机加小批量","机加小批量"),
            new Step("模具样机","模具样机"),
            new Step("模具小批量","模具小批量"),
            new Step("批量生产","批量生产")
        };
    }

    public static class TaskName_Step
    {
        public static IList<Step> list = new List<Step>()
        {
            new Step("--请选择--",""),
            new Step("电气提供参数","电气提供参数"),
            new Step("结构下图","结构下图"),
            new Step("物料采购","物料采购"),
            new Step("制作测试","制作测试"),
            new Step("方案评审","方案评审"),
            new Step("样机总结","样机总结"),
            new Step("归档前评审","归档前评审"),
            new Step("结构图纸归档","结构图纸归档"),
            new Step("工艺文件归档","工艺文件归档"),
            new Step("发货","发货"),
            new Step("设计变更","设计变更"),
            new Step("样机评审资料","样机评审资料"),
            new Step("中试首件","中试首件"),
            new Step("客户需求","客户需求")
        };
    }

    public static class TaskStatus_Step
    {
        public static IList<Step> list = new List<Step>()
        {
            new Step("ALL",""),
            new Step("OPEN","OPEN"),
            new Step("CLOSED","CLOSED"),
            new Step("DELAYCLOSED","DELAYCLOSED"),
            new Step("UNEXPECTCLOSED","UNEXPECTCLOSED")
        };
    }
}