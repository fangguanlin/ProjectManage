﻿using BLL;
using Model;
using System;
using System.Collections.Generic;
using System.Web.Security;
using System.Web.UI;

namespace PMS
{
    public partial class DepartmentManager : System.Web.UI.Page
    {
        private Customer cust;
        private Department dep;
        private UserInDepartment uid;

        public DepartmentManager()
        {
            cust = new Customer();
            dep = new Department();
            uid = new UserInDepartment();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //去掉综合管理部外人员不允许操作的权限，在母版页中直接控制菜单项
                //InvalidUser();
                BindCustomer();
                BindDepartment();
                BindAllUser();
                BindDepartmentDDL();
                BindUser();
            }
        }

        /// <summary>
        /// 非射频综合管理部人员不能编辑
        /// </summary>
        protected void InvalidUser()
        {
            if (!IsValidUser())
            {
                this.Button1.Enabled = false;
                this.Button2.Enabled = false;
                this.Button3.Enabled = false;
                this.Button4.Enabled = false;
                this.Button5.Enabled = false;
                this.Button6.Enabled = false;
                this.LinkButton1.Enabled = false;
            }
        }

        protected void InValidUser1()
        {
            //Response.Write("<script language=javascript>history.go(-1);</script>");
            string str1 = "没有编辑权限！";
            ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str1 + "');</script>", false);
        }

        protected bool IsValidUser()
        {
            foreach (UserInDepartmentInfo uidi in uid.UserInDepartment_GetbyUID(User.Identity.Name))
            {
                //射频综合管理部
                if (uidi.DepartmentID == 9)
                    return true;
            }
            return false;
        }

        #region  客户信息相关操作
        protected void BindCustomer()
        {
            this.ListBox1.DataSource = cust.LoadEntities();
            this.ListBox1.DataTextField = "CustomerName";
            this.ListBox1.DataValueField = "CustomerID";
            this.ListBox1.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string customerName = this.TextBox1.Text.Trim();
            CustomerInfo customerinfo = cust.Customer_GetbyName(customerName);
            if (!string.IsNullOrEmpty(customerinfo.CustomerName))
            {
                string str1 = "此客户信息已存在，添加失败！。";
                ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str1 + "');</script>", false);
            }
            else
            {
                try
                {
                    CustomerInfo custinfo = new CustomerInfo(0, customerName);
                    cust.Insert(custinfo, Page.User.Identity.Name);
                    string str1 = "添加客户信息成功！";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str1 + "');</script>", false);
                    BindCustomer();
                }
                catch
                {
                    string str1 = "添加客户信息失败！请联系管理员。";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str1 + "');</script>", false);
                }
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (this.ListBox1.SelectedIndex != -1)
            {
                try
                {
                    int customerID = int.Parse(this.ListBox1.SelectedValue);
                    cust.Delete(customerID, Page.User.Identity.Name);
                    string str1 = "删除成功！";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str1 + "');</script>", false);
                    BindCustomer();
                }
                catch
                {
                    string str1 = "删除失败！请联系管理员。";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str1 + "');</script>", false);
                }
            }
        }
        #endregion


        #region 部门相关操作
        protected void BindDepartment()
        {
            this.ListBox2.DataSource = dep.LoadEntities();
            this.ListBox2.DataTextField = "DepartmentName";
            this.ListBox2.DataValueField = "DepartmentID";
            this.ListBox2.DataBind();
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            string departmentName = this.TextBox2.Text.Trim();
            DepartmentInfo departmentinfo = dep.Department_GetbyName(departmentName);
            if (!string.IsNullOrEmpty(departmentinfo.DepartmentName))
            {
                string str1 = "此部门信息已存在，添加失败！。";
                ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str1 + "');</script>", false);
            }
            else
            {
                try
                {
                    DepartmentInfo depinfo = new DepartmentInfo(0, departmentName);
                    dep.Insert(depinfo,Page.User.Identity.Name);
                    string str1 = "添加部门信息成功！";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str1 + "');</script>", false);
                    BindDepartment();
                    BindDepartmentDDL();
                }
                catch
                {
                    string str1 = "添加部门信息失败！请联系管理员。";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str1 + "');</script>", false);
                }
            }
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            if (this.ListBox2.SelectedIndex != -1)
            {
                try
                {
                    int departmentID = int.Parse(this.ListBox2.SelectedValue);
                    dep.Delete(departmentID, Page.User.Identity.Name);
                    string str1 = "删除成功！";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str1 + "');</script>", false);
                    BindDepartment();
                    BindDepartmentDDL();
                }
                catch
                {
                    string str1 = "删除失败！请联系管理员。";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str1 + "');</script>", false);
                }
            }
        }
        #endregion


        #region 用户在部门相关操作
        protected void BindAllUser()
        {
            this.ListBox3.DataSource = Membership.GetAllUsers();
            this.ListBox3.DataTextField = "UserName";
            this.ListBox3.DataValueField = "UserName";
            this.ListBox3.DataBind();
        }

        protected void BindDepartmentDDL()
        {
            this.DropDownList1.DataSource = dep.LoadEntities();
            this.DropDownList1.DataTextField = "DepartmentName";
            this.DropDownList1.DataValueField = "DepartmentID";
            this.DropDownList1.DataBind();
        }

        protected void BindUser()
        {
            if (this.DropDownList1.Items.Count > 0)
            {
                int departmentID = int.Parse(this.DropDownList1.SelectedValue);
                this.ListBox4.DataSource = uid.UserInDepartment_GetbyDID(departmentID);
                this.ListBox4.DataTextField = "UserName";
                this.ListBox4.DataValueField = "UserName";
                this.ListBox4.DataBind();
            }
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            if (this.ListBox3.SelectedIndex != -1)
            {
                try
                {
                    string userName = this.ListBox3.SelectedValue;
                    int departmentID = int.Parse(this.DropDownList1.SelectedValue);
                    List<UserInDepartmentInfo> list = (List<UserInDepartmentInfo>)uid.UserInDepartment_GetbyUID(userName);
                    foreach (UserInDepartmentInfo uidinfo in list)
                    {
                        if (uidinfo.DepartmentID == departmentID)
                        {
                            string str2 = "此人员已经在此部门中，无需添加！";
                            ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str2 + "');</script>", false);
                            return;
                        }
                    }
                    UserInDepartmentInfo uidinfo1 = new UserInDepartmentInfo(departmentID, userName);
                    uid.Insert(uidinfo1, Page.User.Identity.Name);
                    BindUser();
                }
                catch
                {
                    string str1 = "添加人员到部门失败！请联系管理员。";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str1 + "');</script>", false);
                }
            }
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            if (this.ListBox4.SelectedIndex != -1)
            {
                try
                {
                    string userName = this.ListBox4.SelectedValue;
                    int departmentID = int.Parse(this.DropDownList1.SelectedValue);
                    UserInDepartmentInfo uidinfo1 = new UserInDepartmentInfo(departmentID, userName);
                    uid.UserInDepartment_Delete(uidinfo1);
                    BindUser();
                }
                catch
                {
                    string str1 = "删除部门中人员失败！请联系管理员。";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str1 + "');</script>", false);
                }
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindUser();
        }
        #endregion

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("~/Account/Register.aspx");
        }
    }
}