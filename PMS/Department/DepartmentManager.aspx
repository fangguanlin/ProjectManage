﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DepartmentManager.aspx.cs" Inherits="PMS.DepartmentManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            text-align: center;
            height: 279px;
            border-top: 1px solid blue;
            border-left: 1px solid blue;
        }

        td {
            border-bottom: 1px solid blue;
            border-right: 1px solid blue;
        }

        .auto-style3 {
            font-size: large;
            color: #0000FF;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style="width: 100%;">
                <br />
                <br />
                <div style="width: 25%; float: left;">
                    <table class="auto-style1">
                        <tr>
                            <td class="auto-style3"><strong>客户信息维护</strong></td>
                        </tr>
                        <tr>
                            <td>客户名称：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1" ErrorMessage="*" ForeColor="Red" ValidationGroup="VGroup1"></asp:RequiredFieldValidator>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="Button1" runat="server" Text="添加" OnClick="Button1_Click" ValidationGroup="VGroup1" /></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ListBox ID="ListBox1" runat="server" Height="147px" Width="167px"></asp:ListBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="Button2" runat="server" Text="删除" OnClick="Button2_Click" OnClientClick="return confirm( '确认删除选中客户吗？'); " /></td>
                        </tr>
                    </table>
                </div>
                <div style="width: 25%; float: left; padding-left: 5%; padding-right: 5%">
                    <table class="auto-style1">
                        <tr>
                            <td class="auto-style3"><strong>部门信息维护</strong></td>
                        </tr>
                        <tr>
                            <td>部门名称：<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox2" ErrorMessage="*" ForeColor="Red" ValidationGroup="VGroup2"></asp:RequiredFieldValidator>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="Button3" runat="server" Text="添加" OnClick="Button3_Click" ValidationGroup="VGroup2" /></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ListBox ID="ListBox2" runat="server" Height="147px" Width="167px"></asp:ListBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="Button4" runat="server" Text="删除" OnClick="Button4_Click" OnClientClick="return confirm( '确认删除选中部门吗？'); " /></td>
                        </tr>
                    </table>
                </div>
                <div style="width: 40%; float: left;">
                    <table class="auto-style1">
                        <tr>
                            <td class="auto-style3" colspan="3"><strong>部门中人员信息维护</strong></td>
                        </tr>
                        <tr>
                            <td>人员列表</td>
                            <td rowspan="2">
                                <asp:Button ID="Button5" runat="server" Text=" &gt; &gt; " OnClick="Button5_Click" />
                                <br />
                                <br />
                                <br />
                                <asp:Button ID="Button6" runat="server" Text=" &lt; &lt; " OnClick="Button6_Click" OnClientClick="return confirm( '确认从部门中删除吗？'); " />
                            </td>
                            <td>选择部门：<br />
                                <asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ListBox ID="ListBox3" runat="server" Height="132px" Width="150px"></asp:ListBox>
                            </td>
                            <td>包含人员：<br />
                                <asp:ListBox ID="ListBox4" runat="server" Height="132px" Width="150px"></asp:ListBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <hr />
            <br />
            <div style=" clear:both; ">
            <p>
                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">注册新用户</asp:LinkButton></p>
            <br />
            <br />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
