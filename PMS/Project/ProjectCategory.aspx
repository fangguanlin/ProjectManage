﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ProjectCategory.aspx.cs" Inherits="PMS.ProjectCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
        <%--<meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Cache-Control" content="no-cache" />
        <meta http-equiv="Expires" content="0" />--%>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
    <script type="text/javascript" src="../Scripts/GetSingleCheckBox.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="style1">
                <tr>
                    <td style=" width:30%;">
                        项目列表：</td>
                    <td style=" width:32%;">
                        <asp:TextBox ID="TextBox1" runat="server" AutoPostBack="True" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
                        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="搜索" />
                    </td>
                    <td style=" width:8%;">
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Project/ProjectAdd.aspx">创建项目</asp:HyperLink>                        
                    </td>
                    <td align="right" style=" width:10%;">
                        <asp:LinkButton ID="LinkButton5" runat="server" OnClick="LinkButton5_Click">设置重点项目</asp:LinkButton>                   
                    </td>
                    <td align="right" style=" width:10%;">
                        <asp:Panel ID="Panel1" runat="server">
                            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click">编辑项目</asp:LinkButton></asp:Panel>
                        <asp:Panel ID="Panel2" runat="server" Visible="false">
                            <asp:LinkButton ID="LinkButton3" runat="server" OnClick="LinkButton3_Click">更新</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton4" runat="server" OnClick="LinkButton4_Click">取消</asp:LinkButton></asp:Panel>
                    </td>
                    <td align="right" style=" width:10%;">
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="return confirm( '删除项目时，与其关联的任务、产品信息也会删掉，你确认要删除？');" OnClick="LinkButton1_Click">删除项目</asp:LinkButton></td>
                </tr>
            </table>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="gridview_m"
                AllowPaging="True" PagerStyle-HorizontalAlign="Center" PageSize="20" 
                OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound" OnRowDeleting="GridView1_RowDeleting"
                DataKeyNames="ProjectID,UserID,RecordTime,IsImportant" OnRowCancelingEdit="GridView1_RowCancelingEdit"
                OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" OnRowCommand="GridView1_RowCommand">
                <Columns>
                    <asp:TemplateField HeaderText="序号">
                        <ItemTemplate>
                            <%# Container.DataItemIndex +1 %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:HyperLinkField DataNavigateUrlFields="ProjectID" DataNavigateUrlFormatString="~/DocTask/ViewDocTaskbyProject.aspx?ProjectID={0}"
                        DataTextField="ProjectID" HeaderText="项目编号" NavigateUrl="~/DocTask/ViewDocTaskbyProject.aspx" />
                    <asp:BoundField DataField="ProjectName" HeaderText="项目名称" />
                    <asp:TemplateField HeaderText="客户名称">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("CustomerID") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("CustomerID") %>'></asp:Label>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="录入时间">
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("RecordTime", "{0:d}") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("RecordTime", "{0:d}") %>'></asp:Label>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="录入人">
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("UserID") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("UserID") %>'></asp:Label>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ProjectTeam" HeaderText="项目成员" />
                    <asp:TemplateField HeaderText="所在阶段">
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("ProjectStep") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:HiddenField ID="HDFProjectStep" runat="server" Value='<%# Eval("ProjectStep") %>' />
                            <asp:DropDownList ID="DDLProjectStep" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="星级">
                        <EditItemTemplate>
                            <asp:TextBox ID="TXBProjectLevel" runat="server" Text='<%# Bind("ProjectLevel") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("ProjectLevel") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="是否重点">
                        <EditItemTemplate>
                            <asp:CheckBox ID="CHBIsImportant" runat="server" Checked='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsImportant")) %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsImportant")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:HyperLinkField DataNavigateUrlFields="ProjectID" DataNavigateUrlFormatString="~/Project/ViewProductTypebyProject.aspx?ProjectID={0}"
                        HeaderText="产品信息" NavigateUrl="~/Project/ViewProductTypebyProject.aspx" Text="项目产品信息" />
                    <asp:CommandField HeaderText="更改" ShowEditButton="True" Visible="False" />
                    <asp:CommandField HeaderText="选择" ShowDeleteButton="True" Visible="False" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox2" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="选择" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Button ID="Button1" runat="server" CausesValidation="false" CommandName="Select" CommandArgument=<%# Eval("ProjectID") %> 
                                Text="完结确认" OnClientClick="return confirm( '确认要完结此项目？'); " />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <PagerSettings Mode="NumericFirstLast" />
                <PagerStyle HorizontalAlign="Center" />
            </asp:GridView>            
            <br />
            <asp:Panel runat="server">
                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Project/ViewProjectClose.aspx">查看完结项目</asp:HyperLink>
            </asp:Panel>
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
