﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using Model;
using PMS.App_Code;

namespace PMS
{
    public partial class ProjectAdd : System.Web.UI.Page
    {
        private Project pro;
        private ProductType pt;
        private Customer cust;
        private DataRow dr;
        //pvtdt有更改时赋值给Session["ProductTypeTable"]
        private DataTable pvtdt
        {
            get { return (DataTable)ViewState["pvtdt"]; }
            set { ViewState["pvtdt"] = value; }
        }

        public ProjectAdd()
        {
            pro = new Project();
            pt = new ProductType();
            cust = new Customer();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindProjectStep();
                BindCustomer();

                pvtdt = new DataTable();
                pvtdt.Columns.Add(new DataColumn("ProductTypeID", Type.GetType("System.String")));
                pvtdt.Columns.Add(new DataColumn("ProductTypeName", Type.GetType("System.String")));
                pvtdt.Columns.Add(new DataColumn("CustomerID", Type.GetType("System.String")));
                pvtdt.Columns.Add(new DataColumn("Remark", Type.GetType("System.String")));
                //Session["ProductTypeTable"] = pvtdt;
            }
            //pvtdt = (DataTable)Session["ProductTypeTable"];
        }

        protected void BindProjectStep()
        {
            this.DropDownList1.DataSource = ProjectProductType_Step.list;
            this.DropDownList1.DataTextField = "StepText";
            this.DropDownList1.DataValueField = "StepValue";
            this.DropDownList1.DataBind();
        }

        protected void BindCustomer()
        {
            this.DropDownList2.DataSource = cust.LoadEntities();
            this.DropDownList2.DataTextField = "CustomerName";
            this.DropDownList2.DataValueField = "CustomerName";
            this.DropDownList2.DataBind();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            dr = pvtdt.NewRow();
            dr["ProductTypeID"] = this.TextBox2.Text.Trim();
            dr["ProductTypeName"] = this.TextBox3.Text.Trim();
            dr["CustomerID"] = this.DropDownList2.SelectedValue;
            dr["Remark"] = this.TextBox6.Text.Trim();
            if (pvtdt.Select("ProductTypeID = '" + this.TextBox2.Text.Trim() + "'").Length == 0)
            {
                pvtdt.Rows.InsertAt(dr, 0);
                //Session["ProductTypeTable"] = pvtdt;
                Bindams();
                this.TextBox2.Text = "";
                this.TextBox3.Text = "";
                this.TextBox6.Text = "";
            }
            else
            {
                string str = "已经在列表中";
                ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
            }
        }

        protected void Bindams()
        {
            this.GridView1.DataSource = pvtdt;
            this.GridView1.DataBind();
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    ((LinkButton)e.Row.Cells[4].Controls[0]).Attributes.Add("onclick", "javascript:return confirm('你确认要删除!')");
                }
            }
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            pvtdt.Select("ProductTypeID = '" + GridView1.Rows[e.RowIndex].Cells[0].Text + "'")[0].Delete();
            //Session["ProductTypeTable"] = pvtdt;
            Bindams();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (!ProductTypeValidate())
            {
                return;
            }
            string projectID = this.TextBox1.Text.Trim();
            if (!ProjectValidate(projectID))
            {
                return;
            }
            string projectName = this.TextBox4.Text.Trim();
            string projectTeam = this.TextBox7.Text.Trim();
            string projectStep = this.DropDownList1.SelectedValue;
            string projectLevel = this.TextBox8.Text.Trim();
            bool isImportant = this.CheckBox1.Checked;
            DateTime dt = DateTime.Now;
            ProjectInfo pvti = new ProjectInfo(projectID, projectName, dt, User.Identity.Name, projectTeam, projectStep,projectLevel,isImportant);
            if (pro.ProjectItems_Insert(pvti, pvtdt))
            {
                this.TextBox1.Text = "";
                this.TextBox4.Text = "";
                this.TextBox7.Text = "";
                pvtdt.Clear();
                //Session["ProductTypeTable"] = pvtdt;
                Bindams();
                this.TextBox1.Focus();
                string str = projectID + " 提交成功!";
                ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
            }
            else
            {
                string str = "提交失败，请联系相关人员!";
                ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
            }
        }

        protected bool ProductTypeValidate()
        {
            if (pvtdt.Rows.Count > 0)
            {
                for (int i = 0; i < pvtdt.Rows.Count; i++)
                {
                    string productTypeID = pvtdt.Rows[i]["ProductTypeID"].ToString();
                    if (!string.IsNullOrEmpty(pt.LoadEntity(productTypeID).ProductTypeID))
                    {
                        string str = "产品代码" + productTypeID + "已经存在，不能重复录入!";
                        ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
                        return false;
                    }
                }
                return true;
            }
            //客户端操作比服务器判断有延时
            //ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>MyFun();</script>", false);
            //if (this.Hidden1.Value == "true")           
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            return true;
        }

        protected bool ProjectValidate(string projectID)
        {
            if (!string.IsNullOrEmpty(pro.LoadEntity(projectID).ProjectID))
            {
                string str = "项目编号" + projectID + "已经存在，不能重复录入！";
                ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
                return false;
            }
            return true;
        }
    }
}