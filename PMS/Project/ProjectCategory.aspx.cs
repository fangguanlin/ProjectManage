﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using Model;
using PMS.App_Code;

namespace PMS
{
    public partial class ProjectCategory : System.Web.UI.Page
    {
        private Project pro;
        private MyProject mp;
        private string searchFactor = "";

        public ProjectCategory()
        {
            pro = new Project();
            mp = new MyProject();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindProject();
            }
        }

        protected void BindProject()
        {
            searchFactor = this.TextBox1.Text.Trim();
            this.GridView1.DataSource = pro.Project_GetbySearch(searchFactor);

            if (Session["gvPageIndex"] == null)
            {
                //Session["gvPageIndex"]为null,即普通的第一次加载页面,不是从维护页返回的.
                if (!Page.IsPostBack)
                {
                    Session["PageIndex"] = "0";
                    this.GridView1.PageIndex = 0; //普通加载,加载第一页数据.
                }
            }
            else if (Session["gvPageIndex"].ToString() == "True")
            {
                //Session["gvPageIndex"]为True,即是从维护页返回的,True值在维护页设置.
                if (!Page.IsPostBack)
                {
                    if (Session["PageIndex"] != null)
                        this.GridView1.PageIndex = Convert.ToInt32(Session["PageIndex"].ToString()); //调用保存在全局变量里的PageIndex值.
                }
                Session["gvPageIndex"] = null; //此处Session值要清空,不然刷新页面就不会在第一页.
            }

            this.GridView1.DataBind();
            this.GridView1.Columns[8].Visible = false;
            this.GridView1.Columns[9].Visible = false;
            this.GridView1.Columns[13].Visible = true;
            if (User.Identity.Name != "邬婕鸣")
            {
                this.GridView1.Columns[14].Visible = false;
            }
            //GridView1.Attributes.Add("BorderColor", "#000000");
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.GridView1.PageIndex = e.NewPageIndex;
            Session["PageIndex"] = e.NewPageIndex.ToString(); //将当前PageIndex的值保存进全局变量gdPageIndex.
            BindProject();
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    ((LinkButton)e.Row.Cells[12].Controls[0]).Attributes.Add("onclick", "javascript:return confirm('删除项目时，与其关联的任务、产品信息也会删掉，你确认要删除!')");
                }
                if (bool.Parse(GridView1.DataKeys[e.Row.RowIndex].Values[3].ToString()))
                {
                    e.Row.Cells[2].ForeColor = CommData.IMPORTANT_COLOR;
                }
                try
                {
                    CheckBox che = (CheckBox)e.Row.FindControl("CheckBox2");
                    che.Attributes.Add("onclick", "ChangeGet(this)");
                }
                catch { }
            }
            if (((DropDownList)e.Row.FindControl("DDLProjectStep")) != null)
            {
                DropDownList ddlprojectStep = (DropDownList)e.Row.FindControl("DDLProjectStep");
                ddlprojectStep.DataSource = ProjectProductType_Step.list;
                ddlprojectStep.DataTextField = "StepText";
                ddlprojectStep.DataValueField = "StepValue";
                ddlprojectStep.DataBind();

                //string s = ((HiddenField)e.Row.FindControl("HDFProjectStep")).Value.Trim();
                //ListItem option = ddlprojectStep.Items.FindByValue(s);
                //if (option != null)
                //{
                //    option.Selected = true;
                //}
                ddlprojectStep.SelectedValue = ((HiddenField)e.Row.FindControl("HDFProjectStep")).Value.Trim();
            }
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            DeleteProject(e.RowIndex);
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            CancelUpdate();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            EditProject(e.NewEditIndex);
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            UpdateProject(e.RowIndex);
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                string projectID = e.CommandArgument.ToString();
                try
                {
                    pro.Project_SetClose(projectID);
                    BindProject();
                }
                catch
                {
                    string str = "项目完结确认失败，请联系管理员！";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
                }
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            ProjectSearch();
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
            ProjectSearch();
        }

        protected void ProjectSearch()
        {
            //searchFactor = this.TextBox1.Text.Trim();
            BindProject();
        }

        //DeleteProject
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)GridView1.Rows[i].FindControl("CheckBox2");
                if (chk != null && chk.Checked)
                {
                    DeleteProject(i);
                    //因为是单选框，检索到选中项后，无须再剩下的搜索
                    return;
                }
            }
        }

        //EditProject
        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)GridView1.Rows[i].FindControl("CheckBox2");
                if (chk != null && chk.Checked)
                {
                    EditProject(i);
                    return;
                }
            }
        }

        //UpdateProject
        protected void LinkButton3_Click(object sender, EventArgs e)
        {
            UpdateProject(this.GridView1.EditIndex);
        }

        //CancelUpdate
        protected void LinkButton4_Click(object sender, EventArgs e)
        {
            CancelUpdate();
        }

        protected void LinkButton5_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)GridView1.Rows[i].FindControl("CheckBox2");
                if (chk != null && chk.Checked)
                {
                    string projectID = GridView1.DataKeys[i].Values[0].ToString();
                    AddMyProject(projectID);
                    return;
                }
            }
        }

        /// <summary>
        /// 删除项目
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteProject(int i)
        {
            if (User.Identity.Name == GridView1.DataKeys[i].Values[1].ToString())
            {
                string projectID = GridView1.DataKeys[i].Values[0].ToString();
                pro.Delete(projectID, Page.User.Identity.Name);
                BindProject();
            }
            else
            {
                string str = "无权删除，只有相应的项目发布人能做此操作！";
                ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
            }
        }

        /// <summary>
        /// 开始编辑
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditProject(int i)
        {
            this.GridView1.EditIndex = i;
            BindProject();
            this.GridView1.Columns[8].Visible = true;
            this.GridView1.Columns[9].Visible = true;
            this.GridView1.Columns[13].Visible = false;

            this.Panel1.Visible = false;
            this.Panel2.Visible = true;
        }

        /// <summary>
        /// 更新项目
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UpdateProject(int i)
        {
            ProjectInfo proInfo = new ProjectInfo();
            proInfo.ProjectID = GridView1.DataKeys[i].Values[0].ToString();
            proInfo.ProjectName = ((TextBox)(GridView1.Rows[i].Cells[2].Controls[0])).Text.Trim().ToString();
            proInfo.UserID = GridView1.DataKeys[i].Values[1].ToString();
            proInfo.ProjectTeam = ((TextBox)(GridView1.Rows[i].Cells[6].Controls[0])).Text.Trim().ToString();
            proInfo.ProjectStep = ((DropDownList)(GridView1.Rows[i].FindControl("DDLProjectStep"))).SelectedValue;
            proInfo.ProjectLevel = ((TextBox)(GridView1.Rows[i].FindControl("TXBProjectLevel"))).Text.Trim();
            proInfo.IsImportant = ((CheckBox)(GridView1.Rows[i].FindControl("CHBIsImportant"))).Checked;

            pro.Update(proInfo, Page.User.Identity.Name);
            GridView1.EditIndex = -1;
            BindProject();

            this.Panel1.Visible = true;
            this.Panel2.Visible = false;
        }

        /// <summary>
        /// 取消编辑
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CancelUpdate()
        {
            this.GridView1.EditIndex = -1;
            BindProject();

            this.Panel1.Visible = true;
            this.Panel2.Visible = false;
        }

        protected void AddMyProject(string projectID)
        {
            MyProjectInfo mpi = new MyProjectInfo(projectID, User.Identity.Name);
            if (!string.IsNullOrEmpty(mp.MyProject_GetbyID(mpi).ProjectID))
            {
                string str = "项目" + projectID + "已经在我的重点项目中！";
                ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
            }
            else
            {
                mp.Insert(mpi, Page.User.Identity.Name);
                string str = "项目" + projectID + "已经加入我的重点项目！";
                ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
            }
        }
    }
}