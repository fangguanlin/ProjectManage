﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewProjectClose.aspx.cs" Inherits="PMS.ViewProjectClose" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
        <br />
        完结项目列表：<br />
        <br />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" CssClass="gridview_m"
        AutoGenerateColumns="False" PageSize="20" Width="90%" 
        onpageindexchanging="GridView1_PageIndexChanging" 
        onrowdatabound="GridView1_RowDataBound" 
        onrowdeleting="GridView1_RowDeleting" DataKeyNames="ProjectID,UserID,RecordTime,IsImportant">
        <Columns>
            <asp:TemplateField HeaderText="序号">
                <itemtemplate>
                    <%# Container.DataItemIndex +1 %>
                </itemtemplate>
            </asp:TemplateField>
            <asp:HyperLinkField DataNavigateUrlFields="ProjectID" 
                DataNavigateUrlFormatString="~/DocTask/ViewDocTaskbyProject.aspx?ProjectID={0}" 
                DataTextField="ProjectID" HeaderText="项目编号" 
                NavigateUrl="~/DocTask/ViewDocTaskbyProject.aspx" />
            <asp:BoundField DataField="ProjectName" HeaderText="项目名称" />
            <asp:BoundField DataField="CustomerID" HeaderText="客户名称" />
            <asp:BoundField DataField="RecordTime" HeaderText="录入时间" 
                DataFormatString="{0:d}" />
            <asp:BoundField DataField="UserID" HeaderText="录入人" />
            <asp:BoundField DataField="ProjectTeam" HeaderText="项目成员" />
            <asp:BoundField DataField="ProjectStep" HeaderText="所在阶段" />
            <asp:HyperLinkField DataNavigateUrlFields="ProjectID" 
                DataNavigateUrlFormatString="~/Project/ViewProductTypebyProject.aspx?ProjectID={0}" 
                HeaderText="产品信息" NavigateUrl="~/Project/ViewProductTypebyProject.aspx" 
                Text="项目产品信息" />
            <asp:CommandField HeaderText="选择" ShowDeleteButton="True" />
        </Columns>
            <PagerStyle HorizontalAlign="Center" />
    </asp:GridView>
        <br />
        <br />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
