﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using Model;
using PMS.App_Code;

namespace PMS
{
    public partial class MyImportantProject : System.Web.UI.Page
    {
        private Project pro;
        private MyProject mp;

        public MyImportantProject()
        {
            pro = new Project();
            mp = new MyProject();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindProject();
            }
        }

        protected void BindProject()
        {
            this.GridView1.DataSource = pro.Project_GetMyProject(User.Identity.Name);
            this.GridView1.DataBind();
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.GridView1.PageIndex = e.NewPageIndex;
            BindProject();
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bool.Parse(GridView1.DataKeys[e.Row.RowIndex].Values[3].ToString()))
                {
                    e.Row.Cells[2].ForeColor = CommData.IMPORTANT_COLOR;
                }
                try
                {
                    CheckBox che = (CheckBox)e.Row.FindControl("CheckBox1");
                    che.Attributes.Add("onclick", "ChangeGet(this)");
                }
                catch { }
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)GridView1.Rows[i].FindControl("CheckBox1");
                if (chk != null && chk.Checked)
                {
                    string projectID = GridView1.DataKeys[i].Values[0].ToString();
                    MyProjectInfo mpi = new MyProjectInfo(projectID, User.Identity.Name);
                    mp.MyProjectDelete(mpi);
                    BindProject();
                    return;
                }
            }
        }
    }
}