﻿using BLL;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PMS
{
    public partial class ViewProjectClose : System.Web.UI.Page
    {
        private Project pro;

        public ViewProjectClose()
        {
            pro = new Project();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindProject();
        }

        protected void BindProject()
        {
            this.GridView1.DataSource = pro.Project_GetbyClose();
            this.GridView1.DataBind();
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.GridView1.PageIndex = e.NewPageIndex;
            BindProject();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (User.Identity.Name == GridView1.DataKeys[e.RowIndex].Values[1].ToString())
            {
                string projectID = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
                pro.Delete(projectID, Page.User.Identity.Name);
                BindProject();
            }
            else
            {
                string str = "无权删除，只有相应的项目发布人能做此操作！";
                ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    ((LinkButton)e.Row.Cells[9].Controls[0]).Attributes.Add("onclick", "javascript:return confirm('你确认要删除!')");
                }
                if (bool.Parse(GridView1.DataKeys[e.Row.RowIndex].Values[3].ToString()))
                {
                    e.Row.Cells[2].ForeColor = System.Drawing.Color.Red;
                }
            }
        }
    }
}