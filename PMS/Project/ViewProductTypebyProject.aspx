﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ViewProductTypebyProject.aspx.cs" Inherits="PMS.ViewProductTypebyProject" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            项目<asp:Label ID="Label1" runat="server"></asp:Label>
            中的产品：<br />
            &nbsp;<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="70%" CssClass="gridview_m"
                 EmptyDataText="此项目与产品无关哟！" DataKeyNames="ProductTypeID"
                OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDataBound="GridView1_RowDataBound"
                OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating"
                OnRowCommand="GridView1_RowCommand">
                <Columns>
                    <asp:BoundField DataField="ProductTypeID" HeaderText="产品代码" ReadOnly="True" />
                    <asp:BoundField DataField="ProductTypeName" HeaderText="产品名称" />
                    <asp:TemplateField HeaderText="客户名称">
                        <EditItemTemplate>
                            <asp:HiddenField ID="HDFCustomerID" runat="server" Value='<%# Eval("CustomerID") %>' />
                            <asp:DropDownList ID="DDLCustomerID" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("CustomerID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="星级">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("ProjectLevel") %>'></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("ProjectLevel") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Remark" HeaderText="备注" />
                    <asp:CommandField HeaderText="更改" ShowEditButton="True" />
                    <asp:CommandField HeaderText="删除" ShowDeleteButton="True" />
                    <asp:ButtonField CommandName="Insert" HeaderText="选择" Text="新增" />
                </Columns>
                <EmptyDataTemplate>
                    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False"
                        DefaultMode="Insert" Height="50px" OnItemInserting="DetailsView1_ItemInserting"
                        OnModeChanging="DetailsView1_ModeChanging" AutoGenerateInsertButton="True">
                        <Fields>
                            <asp:BoundField DataField="ProductTypeID" HeaderText="产品代码" />
                            <asp:BoundField DataField="ProductTypeName" HeaderText="产品名称" />
                            <asp:TemplateField HeaderText="项目编号">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ProjectID") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:Label ID="Label2" runat="server"></asp:Label>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("ProjectID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="客户名称">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CustomerID") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:DropDownList ID="DDLCustomerID" runat="server">
                                    </asp:DropDownList>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("CustomerID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Remark" HeaderText="备注" />
                        </Fields>
                    </asp:DetailsView>
                </EmptyDataTemplate>
            </asp:GridView>
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
