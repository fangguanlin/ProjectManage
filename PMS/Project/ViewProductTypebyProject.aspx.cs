﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using Model;
using PMS.App_Code;

namespace PMS
{
    public partial class ViewProductTypebyProject : System.Web.UI.Page
    {
        private ProductType pro;
        private Customer cust;
        private string projectID
        {
            get { return ViewState["ProjectID"].ToString(); }
            set { ViewState["ProjectID"] = value; }
        }
        private string userID
        {
            get { return ViewState["UserID"].ToString(); }
            set { ViewState["UserID"] = value; }
        }

        public ViewProductTypebyProject()
        {
            pro = new ProductType();
            cust = new Customer();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                projectID = Request.QueryString["ProjectID"].ToString();
                //Session["ProjectID"] = projectID;
                this.Label1.Text = projectID;
                Project proj = new Project();
                userID = proj.LoadEntity(projectID).UserID;
                //Session["UserID"] = userID;
                BindProductType();
                Session["gvPageIndex"] = "True"; //设置Session["gvPageIndex"]为True
            }
            //projectID = Session["ProjectID"].ToString();
            //userID = Session["UserID"].ToString();
        }

        protected void BindProductType()
        {
            this.GridView1.DataSource = pro.GetProductTypesbyProID(projectID);
            this.GridView1.DataBind();
            if (userID != User.Identity.Name)
            {
                this.GridView1.Columns[5].Visible = false;
                this.GridView1.Columns[6].Visible = false;
                this.GridView1.Columns[7].Visible = false;
            }
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            BindProductType();
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (((DetailsView)e.Row.FindControl("DetailsView1")) != null)
            {
                DetailsView d = (DetailsView)e.Row.FindControl("DetailsView1");
                if (((Label)d.Rows[2].Cells[1].FindControl("Label2")) != null)
                {
                    ((Label)d.Rows[2].Cells[1].FindControl("Label2")).Text = projectID;
                }
                if (((DropDownList)d.Rows[3].Cells[1].FindControl("DDLCustomerID")) != null)
                {
                    DropDownList ddlcustomerID1 = (DropDownList)d.Rows[3].Cells[1].FindControl("DDLCustomerID");
                    ddlcustomerID1.DataSource = cust.LoadEntities();
                    ddlcustomerID1.DataTextField = "CustomerName";
                    ddlcustomerID1.DataValueField = "CustomerName";
                    ddlcustomerID1.DataBind();
                }
            }
            if (((DropDownList)e.Row.FindControl("DDLCustomerID")) != null)
            {
                DropDownList ddlCustomerID = (DropDownList)e.Row.FindControl("DDLCustomerID");
                ddlCustomerID.DataSource = cust.LoadEntities();
                ddlCustomerID.DataTextField = "CustomerName";
                ddlCustomerID.DataValueField = "CustomerName";
                ddlCustomerID.DataBind();

                ddlCustomerID.SelectedValue = ((HiddenField)e.Row.FindControl("HDFCustomerID")).Value.Trim();
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    ((LinkButton)e.Row.Cells[6].Controls[0]).Attributes.Add("onclick", "javascript:return confirm('你确认要删除!')");
                }
            }
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string productTypeID = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
            pro.Delete(productTypeID, Page.User.Identity.Name);
            BindProductType();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            BindProductType();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            ProductTypeInfo productTypeInfo = new ProductTypeInfo();//(GridView1.DataKeys[e.RowIndex].Values[0].ToString(), GridView1.DataKeys[e.RowIndex].Values[1].ToString(), GridView1.DataKeys[e.RowIndex].Values[2].ToString(), GridView1.DataKeys[e.RowIndex].Values[3].ToString());
            productTypeInfo.ProductTypeID = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
            productTypeInfo.ProductTypeName = ((TextBox)(GridView1.Rows[e.RowIndex].Cells[1].Controls[0])).Text.Trim().ToString();
            productTypeInfo.ProjectID = projectID;
            productTypeInfo.CustomerID = ((DropDownList)(GridView1.Rows[e.RowIndex].FindControl("DDLCustomerID"))).SelectedValue;
            productTypeInfo.Remark = ((TextBox)(GridView1.Rows[e.RowIndex].Cells[4].Controls[0])).Text.Trim().ToString();

            pro.Update(productTypeInfo, Page.User.Identity.Name);
            GridView1.EditIndex = -1;
            BindProductType();
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Insert")
            {
                GridView1.DataSource = null;
                GridView1.DataBind();
            }
        }

        protected void DetailsView1_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            DetailsView d = (DetailsView)this.GridView1.Controls[0].Controls[0].FindControl("DetailsView1");
            ProductTypeInfo pti = new ProductTypeInfo();
            pti.ProjectID = projectID;
            pti.ProductTypeID = ((TextBox)(d.Rows[0].Cells[1].Controls[0])).Text.Trim();
            pti.ProductTypeName = ((TextBox)(d.Rows[1].Cells[1].Controls[0])).Text.Trim();
            pti.CustomerID = ((DropDownList)(d.Rows[3].Cells[1].FindControl("DDLCustomerID"))).SelectedValue;
            pti.Remark = ((TextBox)(d.Rows[4].Cells[1].Controls[0])).Text.Trim();
            string str = "";
            try
            {
                if (userID != User.Identity.Name)
                {
                    str = "你不项目发布人，没有权限为此项目增加产品信息！";
                }
                else if (!string.IsNullOrEmpty(pro.LoadEntity(pti.ProductTypeID).ProductTypeID))
                {
                    str = "产品代码 " + pti.ProductTypeID + " 已存在，无法添加！";
                }
                else
                {
                    pro.Insert(pti, Page.User.Identity.Name);
                    str = "产品信息添加成功！";
                    BindProductType();
                }
            }
            catch
            {
                str = "任务发布失败，请联系相关人员！";
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.GetType(), "click", "<script>alert('" + str + "');</script>", false);
            }
        }

        protected void DetailsView1_ModeChanging(object sender, DetailsViewModeEventArgs e)
        {
            BindProductType();
        }
    }
}