﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ProjectAdd.aspx.cs" Inherits="PMS.ProjectAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <%--<script type="text/javascript">
    function MyFun() {
        var bln = window.confirm('项目中没有添加产品，是否确定？');
        document.getElementById("<%=Hidden1.ClientID%>").value = bln.toString();
        //alert(document.getElementById("<%=Hidden1.ClientID%>").value);
    }
</script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style="text-align: center;">
                <br />
                <br />
                <br />
                <div style="text-align: center; clear: both; width: 60%; margin: 0 auto;">
                    <table style="width: 100%; border: 1px solid Black;">
                        <tr>
                            <td>
                                项目编号：<asp:TextBox ID="TextBox1" runat="server" TabIndex="1"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1"
                                    ErrorMessage="*" ValidationGroup="VGroup1" ForeColor="Red"></asp:RequiredFieldValidator>&nbsp;
                            </td>
                            <td>
                                项目名称：<asp:TextBox ID="TextBox4" runat="server" TabIndex="1"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBox4"
                                    ErrorMessage="*" ForeColor="Red" ValidationGroup="VGroup1"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                项目成员：<asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBox7"
                                    ErrorMessage="*" ForeColor="Red" ValidationGroup="VGroup1"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                所在阶段：<asp:DropDownList ID="DropDownList1" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>项目星级：<asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                                <%--项目星级设置为可以不输入--%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBox8" ErrorMessage="*" 
                                    ForeColor="Red" ValidationGroup="VGroup1" Enabled="false" ></asp:RequiredFieldValidator>
                            </td>
                            <td>是否重点：<asp:CheckBox ID="CheckBox1" runat="server" />
                            </td>
                            <td>
                                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" TabIndex="2" Text="提  交" ValidationGroup="VGroup1" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="text-align: center; clear: both; width: 60%; border: 1px solid Black;
                    margin: 0 auto; height: 138px;">
                    <br />
                    <br />
                    <table style="width: 100%">
                        <tr>
                            <td>
                                产品代码
                            </td>
                            <td>
                                产品名称
                            </td>
                            <td>
                                客户名称
                            </td>
                            <td>
                                备注
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox2" runat="server" TabIndex="3"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox2"
                                    ErrorMessage="*" ValidationGroup="VGroup2" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBox3" runat="server" TabIndex="4"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox3"
                                    ErrorMessage="*" ValidationGroup="VGroup2" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownList2" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBox6" runat="server" TabIndex="4"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <asp:Button ID="Button2" runat="server" Text="添  加" OnClick="Button2_Click" TabIndex="5"
                        ValidationGroup="VGroup2" />
                </div>
                <div style="text-align: center; clear: both; width: 60%; border: 1px solid Black;
                    margin: 0 auto;">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="gridview_m"
                        OnRowDataBound="GridView1_RowDataBound" OnRowDeleting="GridView1_RowDeleting">
                        <Columns>
                            <asp:BoundField DataField="ProductTypeID" HeaderText="产品代码" />
                            <asp:BoundField DataField="ProductTypeName" HeaderText="产品名称" DataFormatString="{0:N2}" />
                            <asp:BoundField DataField="CustomerID" HeaderText="客户名称" />
                            <asp:BoundField DataField="Remark" HeaderText="备注" />
                            <asp:CommandField HeaderText="选择" ShowDeleteButton="True" />
                        </Columns>
                    </asp:GridView>
                    <br />
                    <br />
                </div>
                <br />
                <br />
                <div style="clear: both;">
                    <input id="Hidden1" type="hidden" runat="server" />
                </div>
                <br />
                <br />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
