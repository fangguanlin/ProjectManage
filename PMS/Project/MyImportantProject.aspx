﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyImportantProject.aspx.cs" Inherits="PMS.MyImportantProject" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="../Scripts/GetSingleCheckBox.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <br />
                <table width="90%">
                    <tr>
                        <td style="width: 50%;">我的重点项目：</td>
                        <td align="right" style="width: 50%;">
                            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">从重点项目中移出</asp:LinkButton></td>
                    </tr>
                </table>
                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" CssClass="gridview_m"
                    AutoGenerateColumns="False" PageSize="20" Width="90%"
                    OnPageIndexChanging="GridView1_PageIndexChanging"
                    OnRowDataBound="GridView1_RowDataBound"
                    DataKeyNames="ProjectID,UserID,RecordTime,IsImportant">
                    <Columns>
                        <asp:TemplateField HeaderText="序号">
                            <ItemTemplate>
                                <%# Container.DataItemIndex +1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:HyperLinkField DataNavigateUrlFields="ProjectID"
                            DataNavigateUrlFormatString="~/DocTask/ViewDocTaskbyProject.aspx?ProjectID={0}"
                            DataTextField="ProjectID" HeaderText="项目编号"
                            NavigateUrl="~/DocTask/ViewDocTaskbyProject.aspx" />
                        <asp:BoundField DataField="ProjectName" HeaderText="项目名称" />
                        <asp:BoundField DataField="CustomerID" HeaderText="客户名称" />
                        <asp:BoundField DataField="RecordTime" HeaderText="录入时间"
                            DataFormatString="{0:d}" />
                        <asp:BoundField DataField="UserID" HeaderText="录入人" />
                        <asp:BoundField DataField="ProjectTeam" HeaderText="项目成员" />
                        <asp:BoundField DataField="ProjectStep" HeaderText="所在阶段" />
                        <asp:HyperLinkField DataNavigateUrlFields="ProjectID"
                            DataNavigateUrlFormatString="~/Project/ViewProductTypebyProject.aspx?ProjectID={0}"
                            HeaderText="产品信息" NavigateUrl="~/Project/ViewProductTypebyProject.aspx"
                            Text="项目产品信息" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle HorizontalAlign="Center" />
                </asp:GridView>
                <br />
                <br />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
