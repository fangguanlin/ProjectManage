﻿using System.Reflection;
using System.Configuration;

namespace DALFactory
{
    public sealed class DataAccess
    {
        private static readonly string path = ConfigurationManager.AppSettings["WebDAL"];

        private DataAccess() { }

        //有需要的时候，这个函数可以单独提取出来建一个类，管理与配置文件相关的各种动态对象的创建、维护
        public static object LocateDALObject(string className)
        {
            string fullPath = path + "." + className;
            return Assembly.Load(path).CreateInstance(fullPath);
        }

        public static IDAL.IProductType CreateProductType()
        {
            return (IDAL.IProductType)LocateDALObject("ProductType");
        }

        public static IDAL.IProject CreateProject()
        {
            return (IDAL.IProject)LocateDALObject("Project");
        }

        public static IDAL.IDocTask CreateDocTask()
        {
            return (IDAL.IDocTask)LocateDALObject("DocTask");
        }

        public static IDAL.IDocTaskChange CreateDocTaskChange()
        {
            return (IDAL.IDocTaskChange)LocateDALObject("DocTaskChange");
        }

        public static IDAL.IDocTaskCheck CreateDocTaskCheck()
        {
            return (IDAL.IDocTaskCheck)LocateDALObject("DocTaskCheck");
        }

        public static IDAL.ICustomer CreateCustomer()
        {
            return (IDAL.ICustomer)LocateDALObject("Customer");
        }

        public static IDAL.IDepartment CreateDepartment()
        {
            return (IDAL.IDepartment)LocateDALObject("Department");
        }

        public static IDAL.IUserInDepartment CreateUserInDepartment()
        {
            return (IDAL.IUserInDepartment)LocateDALObject("UserInDepartment");
        }

        public static IDAL.IMyProject CreateMyProject()
        {
            return (IDAL.IMyProject)LocateDALObject("MyProject");
        }
    }
}