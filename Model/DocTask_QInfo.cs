﻿
namespace Model
{
    public class DocTask_QInfo
    {
        public DocTask_QInfo() { }

        public DocTask_QInfo(string executor, decimal quantity, decimal quantity1, int element, int element1, int total, int element2, int element3, int element4)
        {
            this.Executor = executor;
            this.Quantity = quantity;
            this.Quantity1 = quantity1;
            this.Element = element;
            this.Element1 = element1;
            this.Total = total;
            this.Element2 = element2;
            this.Element3 = element3;
            this.Element4 = element4;
        }

        public string Executor { get; set; }
        public decimal Quantity { get; set; }
        public decimal Quantity1 { get; set; }
        public int Element { get; set; }
        public int Element1 { get; set; }
        public int Element2 { get; set; }
        public int Element3 { get; set; }
        public int Element4 { get; set; }
        public int Total { get; set; }
    }
}
