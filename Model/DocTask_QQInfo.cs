﻿
namespace Model
{
    public class DocTask_QQInfo
    {
        public DocTask_QQInfo() { }

        public DocTask_QQInfo(string executor, decimal quarter1, decimal quarter2, decimal quarter3, decimal quarter4)
        {
            this.Executor = executor;
            this.第1季度 = quarter1;
            this.第2季度 = quarter2;
            this.第3季度 = quarter3;
            this.第4季度 = quarter4;
        }

        public string Executor { get; set; }
        public decimal 第1季度 { get; set; }
        public decimal 第2季度 { get; set; }
        public decimal 第3季度 { get; set; }
        public decimal 第4季度 { get; set; }
    }
}
