﻿
namespace Model
{
    public class CustomerInfo
    {
        public CustomerInfo() { }
        public CustomerInfo(int customerID, string customerName)
        {
            this.CustomerID = customerID;
            this.CustomerName = customerName;
        }
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
    }
}
