﻿
namespace Model
{
    public class ProductTypeInfo
    {
        public ProductTypeInfo() { }
        public ProductTypeInfo(string productTypeID, string productTypeName, string projectID, string customerID, string remark)
        {
            this.ProductTypeID = productTypeID;
            this.ProductTypeName = productTypeName;
            this.ProjectID = projectID;
            this.CustomerID = customerID;
            this.Remark = remark;
        }
        public ProductTypeInfo(string productTypeID, string productTypeName, string projectID, string customerID, string remark,string projectLevel)
        {
            this.ProductTypeID = productTypeID;
            this.ProductTypeName = productTypeName;
            this.ProjectID = projectID;
            this.CustomerID = customerID;
            this.Remark = remark;
            this.ProjectLevel = projectLevel;
        }

        public string ProductTypeID { get; set; }
        public string ProductTypeName { get; set; }
        public string ProjectID { get; set; }
        public string CustomerID { get; set; }
        public string Remark { get; set; }

        public string ProjectLevel { get; set; }
    }
}
