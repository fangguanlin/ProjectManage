﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class MyProjectInfo
    {
        public MyProjectInfo() { }

        public MyProjectInfo(string projectID, string userID)
        {
            this.ProjectID = projectID;
            this.UserID = userID;
        }

        public string ProjectID { get; set; }
        public string UserID { get; set; }
    }
}
