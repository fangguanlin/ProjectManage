﻿using System;

namespace Model
{
    public class DocTaskChangeInfo
    {
        public DocTaskChangeInfo() { }
        public DocTaskChangeInfo(int taskID, DateTime recordTime, string userID, string changeDetail)
        {
            this.TaskID = taskID;
            this.RecordTime = recordTime;
            this.UserID = userID;
            this.ChangeDetail = changeDetail;
        }

        public int TaskID { get; set; }
        public DateTime RecordTime { get; set; }
        public string UserID { get; set; }
        public string ChangeDetail { get; set; }
    }
}
