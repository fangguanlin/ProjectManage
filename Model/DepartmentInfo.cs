﻿
namespace Model
{
    public class DepartmentInfo
    {
        public DepartmentInfo() { }
        public DepartmentInfo(int departmentID, string departmentName)
        {
            this.DepartmentID = departmentID;
            this.DepartmentName = departmentName;
        }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
    }
}
