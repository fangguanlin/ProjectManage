﻿using System;

namespace Model
{
    public class DocTaskCheckInfo
    {
        public DocTaskCheckInfo() { }
        public DocTaskCheckInfo(int taskID, DateTime recordTime, string userID, DateTime endTime, string closeStatus)
        {
            this.TaskID = taskID;
            this.RecordTime = recordTime;
            this.UserID = userID;
            this.EndTime = endTime;
            this.CloseStatus = closeStatus;
        }

        public int TaskID { get; set; }
        public DateTime RecordTime { get; set; }
        public string UserID { get; set; }
        public DateTime EndTime { get; set; }
        public string CloseStatus { get; set; }
    }
}
