﻿using System;

namespace Model
{
    public class ProjectInfo
    {
        public ProjectInfo() { }
        public ProjectInfo(string projectID, string projectName, DateTime recordTime, string userID, string projectTeam, string projectStep
            , string projectLevel, bool isImportant)
        {
            this.ProjectID = projectID;
            this.ProjectName = projectName;
            this.RecordTime = recordTime;
            this.UserID = userID;
            this.ProjectTeam = projectTeam;
            this.ProjectStep = projectStep;
            this.ProjectLevel = projectLevel;
            this.IsImportant = isImportant;
        }

        public ProjectInfo(string projectID, string projectName,string customerID, DateTime recordTime, string userID,string projectTeam,string ProjectStep
            ,string projectLevel,bool isImportant)
        {
            this.ProjectID = projectID;
            this.ProjectName = projectName;
            this.CustomerID = customerID;
            this.RecordTime = recordTime;
            this.UserID = userID;
            this.ProjectTeam = projectTeam;
            this.ProjectStep = ProjectStep;
            this.ProjectLevel = projectLevel;
            this.IsImportant = isImportant;
        }

        public string ProjectID { get; set; }
        public string ProjectName { get; set; }
        public DateTime RecordTime { get; set; }
        public string UserID { get; set; }
        public string ProjectTeam { get; set; }
        public string ProjectStep { get; set; }
        public string ProjectLevel { get; set; }
        public bool IsImportant { get; set; }
        public string ProjectStatus { get; set; }

        public string CustomerID { get; set; }
    }
}
