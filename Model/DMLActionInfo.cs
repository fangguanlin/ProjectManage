﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class DMLActionInfo
    {
        public DMLActionInfo() { }

        public DMLActionInfo(DateTime recordTime,string userID,string sPName,string strparams)
        {
            this.RecordTime = recordTime;
            this.UserID = userID;
            this.SPName = sPName;
            this.Params = strparams;
        }
        public DateTime RecordTime { get; set; }
        public string UserID { get; set; }
        public string SPName { get; set; }
        public string Params { get; set; }
    }
}
