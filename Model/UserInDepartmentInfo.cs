﻿using System;

namespace Model
{
    public class UserInDepartmentInfo
    {
        public UserInDepartmentInfo() { }
        public UserInDepartmentInfo(int departmentID, string userName)
        {
            this.DepartmentID = departmentID;
            this.UserName = userName;
        }
        public int DepartmentID { get; set; }
        public string UserName { get; set; }
    }
}
