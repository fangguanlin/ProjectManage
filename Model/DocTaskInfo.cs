﻿using System;

namespace Model
{
    public class DocTaskInfo
    {
        public DocTaskInfo() { }
        public DocTaskInfo(int taskID, string projectID, string taskName, string productTypeStep, DateTime recordTime, string publisher, DateTime beginTime, DateTime postTime, 
            string executor, string department, string taskDetail,DateTime endTime,int consumeDays, string taskStatus)
        {
            this.TaskID = taskID;
            this.ProjectID = projectID;
            this.TaskName = taskName;
            this.ProductTypeStep = productTypeStep;
            this.RecordTime = recordTime;
            this.Publisher = publisher;
            this.BeginTime = beginTime;
            this.PostTime = postTime;
            this.Executor = executor;
            this.Department = department;
            this.TaskDetail = taskDetail;
            this.EndTime = endTime;
            this.ConsumeDays = consumeDays;
            this.TaskStatus = taskStatus;
        }

        public DocTaskInfo(int taskID, string projectID, string projectName, string taskName, string productTypeStep, DateTime recordTime, string publisher, DateTime beginTime, 
            DateTime postTime, string executor, string department, string taskDetail, DateTime endTime,int consumeDays, string taskStatus,string closeStatus,bool isImportant)
        {
            this.TaskID = taskID;
            this.ProjectID = projectID;
            this.ProjectName = projectName;
            this.TaskName = taskName;
            this.ProductTypeStep = productTypeStep;
            this.RecordTime = recordTime;
            this.Publisher = publisher;
            this.BeginTime = beginTime;
            this.PostTime = postTime;
            this.Executor = executor;
            this.Department = department;
            this.TaskDetail = taskDetail;
            this.EndTime = endTime;
            this.ConsumeDays = consumeDays;
            this.TaskStatus = taskStatus;
            this.CloseStatus = closeStatus;
            this.IsImportant = isImportant;
        }

        public int TaskID { get; set; }
        public string ProjectID { get; set; }
        public string TaskName { get; set; }
        public string ProductTypeStep { get; set; }
        public DateTime RecordTime { get; set; }
        public string Publisher { get; set; }
        public DateTime BeginTime { get; set; }
        public DateTime PostTime { get; set; }
        public string Executor { get; set; }
        public string Department { get; set; }
        public string TaskDetail { get; set; }
        public DateTime EndTime { get; set; }
        public int ConsumeDays { get; set; }
        public string TaskStatus { get; set; }

        public string ProjectName { get; set; }
        public bool IsImportant { get; set; }
        public string CloseStatus { get; set; }
    }
}
