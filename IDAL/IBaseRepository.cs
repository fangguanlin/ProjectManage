﻿using System.Collections.Generic;

namespace IDAL
{
    public interface IBaseRepository<T> where T : class, new()
    {
        void Insert(T Entity,string userID);
        void Update(T Entity, string userID);
        void Delete<T1>(T1 Id, string userID);
        //T1 may be <int> or <string> 
        T LoadEntity<T1>(T1 Id);
        IList<T> LoadEntities();
    }
}