﻿using System;
using System.Collections.Generic;
using Model;

namespace IDAL
{
    public interface IDocTask : IBaseRepository<DocTaskInfo>
    {
        DocTaskInfo GetDocTaskbyPIDName(string projectID, string taskName);
        IList<DocTaskInfo> GetDocTasksbyPID(string projectID);
        IList<DocTaskInfo> GetDocTasksbyPIDClose(string projectID);
        IList<DocTaskInfo> GetDocTasksbyExe(string executor);
        IList<DocTaskInfo> GetCloseDocTasks();//
        IList<DocTaskInfo> SearchDocTaskbyAll(string projectID, string publisher, string executor, string department, string projectName, string productTypeID
            , string customerID, string taskStatus, string taskName, string productTypeStep,DateTime dt1,DateTime dt2);
        IList<DocTask_QQInfo> CountDocTask_AllQ(string yearTime);
        IList<DocTask_QInfo> CountDocTasks_Q(string quarterTime);
        IList<DocTask_QQInfo> CountDocTasks_Customer_AllQ(string yearTime);
        IList<DocTask_QInfo> CountDocTasks_Customer_Q(string quarterTime);
        IList<DocTask_QInfo> CountDocTasks_DT(DateTime starttime, DateTime stoptime);
        IList<DocTask_QInfo> CountDocTasks_Customer_DT(DateTime starttime, DateTime stoptime);
    }
}
