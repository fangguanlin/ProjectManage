﻿using System;
using System.Data;
using System.Collections.Generic;
using Model;

namespace IDAL
{
    public interface IUserInDepartment : IBaseRepository<UserInDepartmentInfo>
    {
        void UserInDepartment_Delete(UserInDepartmentInfo uidinfo);
        IList<UserInDepartmentInfo> UserInDepartment_GetbyDID(int departmentID);
        IList<UserInDepartmentInfo> UserInDepartment_GetbyUID(string userName);
    }
}
