﻿using System.Collections.Generic;
using Model;

namespace IDAL
{
    public interface IDocTaskChange : IBaseRepository<DocTaskChangeInfo>
    {
        IList<DocTaskChangeInfo> GetDocTaskChangesbyID(int taskID);
    }
}
