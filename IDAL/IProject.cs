﻿using System.Data;
using System.Collections.Generic;
using Model;

namespace IDAL
{
    public interface IProject : IBaseRepository<ProjectInfo>
    {
        IList<ProjectInfo> Project_GetbyClose();
        bool ProjectItems_Insert(ProjectInfo proi, DataTable itemdt);

        void Project_SetClose(string projectID);

        IList<ProjectInfo> Project_GetMyProject(string userID);

        IList<ProjectInfo> Project_GetbySearch(string searchFactor);
    }
}
