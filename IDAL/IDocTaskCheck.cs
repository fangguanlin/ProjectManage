﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace IDAL
{
    public interface IDocTaskCheck : IBaseRepository<DocTaskCheckInfo>
    {
        void Insert_Unexpect(DocTaskCheckInfo doc);
    }
}
