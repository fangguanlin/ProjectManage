﻿using System.Data;
using System.Collections.Generic;
using Model;

namespace IDAL
{
    public interface IDepartment : IBaseRepository<DepartmentInfo>
    {
        DepartmentInfo Department_GetbyName(string departmentName);
    }
}
