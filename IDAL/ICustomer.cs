﻿using System.Data;
using System.Collections.Generic;
using Model;

namespace IDAL
{
    public interface ICustomer : IBaseRepository<CustomerInfo>
    {
        CustomerInfo Customer_GetbyName(string customerName);
    }
}
