﻿using System.Collections.Generic;
using Model;

namespace IDAL
{
    public interface IProductType : IBaseRepository<ProductTypeInfo>
    {
        IList<ProductTypeInfo> GetProductTypesbyProID(string projectID);
    }
}
