﻿using System.Data;
using System.Collections.Generic;
using Model;

namespace IDAL
{
    public interface IMyProject : IBaseRepository<MyProjectInfo>
    {
        void MyProjectDelete(MyProjectInfo mpi);
        MyProjectInfo MyProject_GetbyID(MyProjectInfo mpi);
    }
}
